#!/bin/bash

echo $DISPLAY

if [[ -z "$DISPLAY" ]]; then 
	export DISPLAY=:1
fi
bmrbid=$1
###################################################
# produce graphics of predicted secondary structure
###################################################
/usr/local/bin/Rscript /apps/csi3/project/project/Standalone_CSI3.0/src/output_graphics_csi3.r $bmrbid

