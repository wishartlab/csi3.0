#!/usr/bin/python

from __future__ import division
import sys
#==================================================
# This python script takes a talosplus predicted
# torsion angle and identifies different types of
# (type I, type Ip, type II, type IIp) beta-turns
# and classifies them, identifies external and
# internal beta-strands based on accessible surface 
# area class and finally locates the beta-hairpins
#===================================================

inFile = sys.argv[1]
angleFile = inFile+".anglepred"
ssFile = inFile+".sspred1"
asaFile = inFile+".asapred"
#tabFile = inFile+".tab"
outFile = inFile+".csi3"

resmap = {'A' : 'ALA','R' : 'ARG','N' : 'ASN','D' : 'ASP','B' : 'ASX','C' : 'CYS', 'a':'CYS', 'b':'CYS', 'c':'CYS', 'Q' : 'GLN','E' : 'GLU','Z' : 'GLX','G' : 'GLY','H' : 'HIS','I' : 'ILE','L' : 'LEU','K' : 'LYS','M' : 'MET','F' : 'PHE','P' : 'PRO','S' : 'SER','T' : 'THR','W' : 'TRP','Y' : 'TYR','V' : 'VAL','X' : 'PCA'}

three2one ={'ALA' : 'A','ARG' : 'R','ASN' : 'N','ASP' : 'D','ASX' : 'B','CYS' : 'C','GLN' : 'Q','GLU' : 'E','GLX' : 'Z','GLY' : 'G','HIS' : 'H','ILE' : 'I','LEU' : 'L','LYS' : 'K','MET' : 'M','PHE' : 'F','PRO' : 'P','SER' : 'S','THR' : 'T','TRP' : 'W','TYR' : 'Y','VAL' : 'V','PCA' : 'X'}

bturnAAPositionalPreference={'I': [['D','N','C','S', 'P', 'H'], ['P', 'E', 'S'], ['D', 'N', 'S', 'T'], ['G']], 'II': [['P', 'Y'], ['K', 'P'], ['G', 'N'], ['S', 'K', 'C']], 'VIII': [['P', 'G'], ['P', 'D'], [ 'N', 'D', 'V'], ['P', 'T']], 'Ip': [['Y', 'I', 'V'], ['N', 'D','G','H'], [ 'G'], ['K']], 'IIp': [['Y'], ['G'], [ 'N', 'D', 'S'], ['G', 'T']] }

bturnAllPreferredRes = ['D', 'N', 'H', 'P', 'S', 'E', 'T', 'C', 'Y','K', 'I', 'V', 'G']

# [polar, special, apolar]
residue_groups_by_polarity = [['K', 'R', 'H', 'E', 'D', 'S', 'N', 'Q', 'A'], ['C', 'G', 'P'], ['F', 'M', 'I', 'L','V', 'W','C', 'Y', 'T', 'A']]

ssDict = {}
ssDictOrig={}
phiDict = {}
psiDict = {}
asaDict = {}
HAsecshiftDict = {}
HNsecshiftDict = {}
ssSegmentDict = {}
ssSegmentResidueDict = {}
ssSegmentHADict = {}
ssSegmentHNDict = {}
ssSegments = []
ssSegmentResNum = {}
resn_res_array=[]


def IN (angle, target):
	a1 = angle
	a2 = target
	while(a1 < -180):
		a1 +=360
	while(a1 >= 180):
		a1 -=360
	while(a2 < -180):
		a2 +=360
	while(a2>=180):
		a2 -=360
	if(a1-a2) >= 0:
		if a1-a2 < 180:
			pass
		else:
			a2+=360
	else:
		if a1-a2 > -180:
			pass
		else:
			a1+=360
	return abs(a1-a2)		
def isSubSet (List, subList):
	count = 0
	for elem in subList:
		if elem not in List:
			count +=1 
			#return False
	#return True
	# return true if ateast 3 out 4 residues are preferred for b-turn conformation
	if count == 0:
		return True
	else:
		return False

def read_file():
	fss = open(ssFile, "r")
	fangle = open(angleFile, "r")
	fasa = open(asaFile, "r")
	#ftab = open(tabFile, "r")

	
	# reading secondary chemical shift infromation
	#for line in ftab:
		#if line[0].isdigit():
			#(resn, residue, atom, d,d, seccsshift) = line.split()
			#if atom == "HA":
				#HAsecshiftDict[resn+'_'+residue] = round(float(seccsshift),2)
			#if atom == "HN":
				#HNsecshiftDict[resn+'_'+residue] = round(float(seccsshift),2)
	for line in fss:
		if line.strip() and not line.startswith("#"):
			(resn, residue, ss) = line.split()
			#(dummy, resn, residue, dummy, ss) = line.split()
			ssDict[resn+'_'+resmap[residue]] = ss
			ssDictOrig[resn+'_'+resmap[residue]] = ss

	for line in fasa:
		if not line.startswith("#"):
			(resn, residue, asa) = line.split()#.asapred extension
			#(d, resn, residue, d, asa) = line.split()#.asapred2 extension
			asaDict[resn+'_'+residue] = round(float(asa), 2) 		
	for line in fangle:
		line= line.strip()
		if line.strip() and line[0].isdigit():
			elems = line.split()
			resn = elems[0]
			residue = elems[1]
			phi = elems[2]
			psi = elems[3]
			resn_res_array.append(resn+'_'+resmap[residue])
			
			#if float(phi) == 9999.000 or float(psi) == 9999.000:
			#	continue
			phiDict[resn+'_'+resmap[residue]] = float(phi)
			psiDict[resn+'_'+resmap[residue]] = float(psi) 
def calc_ss_segments():
	num = 0
	segmentStart = 0
	
	
	
	for i in range(len(resn_res_array)-1):
		
		current = resn_res_array[i]
		current_res = three2one [current.split("_")[1]]
		next = resn_res_array[i+1]
		
		
		if current in ssDict and next not in ssDict:
			if segmentStart:
				ssSegmentDict[ssDict[current]+"."+str(num)] += [current]
				ssSegmentResidueDict[ssDict[current]+"."+str(num)] += [current_res]
				if current in HAsecshiftDict:
					ssSegmentHADict[ssDict[current]+"."+str(num)] += [HAsecshiftDict[current]]
				 
				segmentStart = 0
			elif not segmentStart:
				num+=1
				ssSegmentDict[ssDict[current]+"."+str(num)] = [current]
				ssSegmentResidueDict[ssDict[current]+"."+str(num)] = [current_res]
				if current in HAsecshiftDict:
					ssSegmentHADict[ssDict[current]+"."+str(num)] = [HAsecshiftDict[current]]
		
		if current in ssDict and next in ssDict:
			 
			
			if not segmentStart:
				num += 1
				
				ssSegmentDict[ssDict[current]+"."+str(num)] = [current]
				ssSegmentResidueDict[ssDict[current]+"."+str(num)] = [current_res]
				if current in HAsecshiftDict:
					ssSegmentHADict[ssDict[current]+"."+str(num)] = [HAsecshiftDict[current]]
				
				
				segmentStart = 1 
				# if a ss segment is of 1 residue length 
				if ssDict[current] != ssDict[next] and segmentStart:
				
					ssSegmentDict[ssDict[current]+"."+str(num)] += [current]
					ssSegmentResidueDict[ssDict[current]+"."+str(num)] += [current_res]
					if current in HAsecshiftDict:
						ssSegmentHADict[ssDict[current]+"."+str(num)] += [HAsecshiftDict[current]]
					
					segmentStart = 0
			elif ssDict[current] == ssDict[next] and (segmentStart==1):
				ssSegmentDict[ssDict[current]+"."+str(num)] += [current]
				
				
				ssSegmentResidueDict[ssDict[current]+"."+str(num)] += [current_res]
				if current in HAsecshiftDict:
					ssSegmentHADict[ssDict[current]+"."+str(num)] += [HAsecshiftDict[current]]
				
				
			elif ssDict[current] != ssDict[next] and segmentStart:
				
				ssSegmentDict[ssDict[current]+"."+str(num)] += [current]
				ssSegmentResidueDict[ssDict[current]+"."+str(num)] += [current_res]
				if current in HAsecshiftDict:
					ssSegmentHADict[ssDict[current]+"."+str(num)] += [HAsecshiftDict[current]]
				
				segmentStart = 0
	return num			
	 	
def calc_bturn():
	
	bounds = []
	bounds += [40.0]
	bounds += [40.0]
	bounds += [40.0]
	bounds += [40.0]
	for key in ssSegmentDict:
		bturn = 0
		if key.startswith("C"):
			
			resnList = ssSegmentDict[key]
			residueList = ssSegmentResidueDict[key]
			
			
			if len(resnList) > 4:
				i = 0
				while( i < len(resnList)-3):
					
					residueL = residueList[i:i+4]	
					
					phi1, psi1 = phiDict[resnList[i+1]], psiDict[resnList[i+1]]
					phi2, psi2 = phiDict[resnList[i+2]], psiDict[resnList[i+2]]
					
					if residueL[1] == "G" or residueL[2] == "G":
						bounds += [50.0]
						bounds += [50.0]
						bounds += [45.0]
						bounds += [45.0]
						
					
					if ( (phi1>=(-60-bounds[0]) and phi1<= (-60 +bounds[0])) and (psi1>=(-30-bounds[1]) and psi1<= (-30 +bounds[1])) and (phi2>=(-90-bounds[2]) and phi2 <= (-90 +bounds[2]) ) and (psi2>=(0-bounds[3]) and psi2<= (0 +bounds[3]) )) :
					
						# I
						turnAssigned  = "T1"
						bturn = 1
							
							
					elif ((phi1>=(60-bounds[0]) and phi1<= (60 +bounds[0])) and (psi1>=(30-bounds[1]) and psi1<= (30 +bounds[1])) and (phi2>=(90-bounds[2]) and phi2 <= (90 +bounds[2]) ) and (psi2>=(0-bounds[3]) and psi2<= (0 +bounds[3]))):
					
						# I'
						turnAssigned =  "T1p"
						bturn = 1
						
					elif ((phi1>=(-60-bounds[0]) and phi1<= (-60 +bounds[0])) and (psi1>=(120-bounds[1]) and psi1<= (120 +bounds[1])) and (phi2>=(80-bounds[2]) and phi2 <= (80 +bounds[2]) ) and (psi2>=(0-bounds[3]) and psi2<= (0 +bounds[3]) )) :
					
						#II
						turnAssigned=  "T2"
						bturn = 1
						
					elif ((phi1>=(60-bounds[0]) and phi1<= (60 +bounds[0])) and (psi1>=(-120-bounds[1]) and psi1<= (-120 +bounds[1])) and (phi2>=(-80-bounds[2]) and phi2 <= (-80 +bounds[2]) ) and (psi2>=(0-bounds[3]) and psi2<= (0 +bounds[3]) )) :
					
						# II'
						turnAssigned = "T2p"
						bturn = 1
						
					elif ((phi1>=(-60-bounds[0]) and phi1<= (-60 +bounds[0])) and (psi1>=(-30-bounds[1]) and psi1<= (-30 +bounds[1])) and (phi2>=(-120-bounds[2]) and phi2 <= (-120 +bounds[2]) ) and (psi2>=(120-bounds[3]) and psi2<= (120 +bounds[3]) )) :
					
						# VIII
						turnAssigned  = "T8"
						bturn = 1
						
				
					if bturn and isSubSet ( bturnAllPreferredRes, residueL) ==True:
						#print("Assigning turn type to beta-turn.......")
						for j in range (i, i+4):
							ssDict[resnList[j]] = turnAssigned
						i = i+4
					else:
						i = i+1
			
			elif len(resnList) > 3 and len(resnList) <= 4 :
				
				phi1, psi1 = phiDict[resnList[1]], psiDict[resnList[1]]
				phi2, psi2 = phiDict[resnList[2]], psiDict[resnList[2]]
				
				if residueList[1] == "G" or residueList[2] == "G":
						bounds[0] = 50.0
						bounds[1] = 50.0
						bounds[2] = 45.0
						bounds[3] = 45.0
				
				if ( (phi1>=(-60-bounds[0]) and phi1<= (-60 +bounds[0])) and (psi1>=(-30-bounds[1]) and psi1<= (-30 +bounds[1])) and (phi2>=(-90-bounds[2]) and phi2 <= (-90 +bounds[2]) ) and (psi2>=(0-bounds[3]) and psi2<= (0 +bounds[3]) )) :
					
						# I
						turnAssigned  = "T1"
						bturn = 1
							
							
				elif ((phi1>=(60-bounds[0]) and phi1<= (60 +bounds[0])) and (psi1>=(30-bounds[1]) and psi1<= (30 +bounds[1])) and (phi2>=(90-bounds[2]) and phi2 <= (90 +bounds[2]) ) and (psi2>=(0-bounds[3]) and psi2<= (0 +bounds[3]))):
				
						# I'
						turnAssigned =  "T1p"
						bturn = 1
						
				elif ((phi1>=(-60-bounds[0]) and phi1<= (-60 +bounds[0])) and (psi1>=(120-bounds[1]) and psi1<= (120 +bounds[1])) and (phi2>=(80-bounds[2]) and phi2 <= (80 +bounds[2]) ) and (psi2>=(0-bounds[3]) and psi2<= (0 +bounds[3]) )) :
				
						#II
						turnAssigned=  "T2"
						bturn = 1
						
				elif ((phi1>=(60-bounds[0]) and phi1<= (60 +bounds[0])) and (psi1>=(-120-bounds[1]) and psi1<= (-120 +bounds[1])) and (phi2>=(-80-bounds[2]) and phi2 <= (-80 +bounds[2]) ) and (psi2>=(0-bounds[3]) and psi2<= (0 +bounds[3]) )) :
				
						# II'
						turnAssigned = "T2p"
						bturn = 1
						
				elif ((phi1>=(-60-bounds[0]) and phi1<= (-60 +bounds[0])) and (psi1>=(-30-bounds[1]) and psi1<= (-30 +bounds[1])) and (phi2>=(-120-bounds[2]) and phi2 <= (-120 +bounds[2]) ) and (psi2>=(120-bounds[3]) and psi2<= (120 +bounds[3]) )) :
				
						# VIII
						turnAssigned  = "T8"
						bturn = 1
					
				if bturn and isSubSet ( bturnAllPreferredRes, residueList) ==True:
					#print("Assigning turn type to beta-turn.......")
					for i in range( len(resnList)):
						ssDict[resnList[i]] = turnAssigned		

def detect_periodicity_sec_cs(secshiftList):
	periodicity = 0
	
	for i in range (1,len(secshiftList)-1, 2):
		
		prev_shift = secshiftList[i-1]
		current_shift = secshiftList[i]
		next_shift = secshiftList[i+1]
		if (current_shift < prev_shift and next_shift > current_shift) or (current_shift > prev_shift and next_shift < current_shift):
			periodicity = 1
		else:
			periodicity = 0
			break
		
	return periodicity	
		
def check_altering_hydrophilic_hydrophobic(residueList):
	residue_polarity = []
	ext_strand_flag = 0
	for residue in residueList:
		if residue in residue_groups_by_polarity[0]:
			residue_polarity += ["polar"]
		elif residue in residue_groups_by_polarity[1] or residue in residue_groups_by_polarity[2]:
			residue_polarity+=["apolar"]
			
	for i in range( 1, len(residue_polarity)-1, 1):
		prev = residue_polarity[i-1]
		current = residue_polarity[i]
		next = residue_polarity[i+1]
		
		#print(prev, current, next)
		if (prev == "polar" and current == "apolar" and next == "polar") or (prev == "apolar" and current == "polar" and next == "apolar"):
			ext_strand_flag = 1
	
		elif (prev == "polar" and current == "apolar" and next == "apolar") or (prev == "apolar" and current == "polar" and next == "polar") or (prev == "polar" and current == "polar" and next == "apolar") or (prev == "apolar" and current == "apolar" and next == "polar") :
			ext_strand_flag = 1
		elif (prev == "apolar" and current == "apolar" and next == "apolar") or (prev == "polar" and current == "polar" and next == "polar"):
			ext_strand_flag = 0
			break
	return ext_strand_flag 		
			
def calc_ext_int_bstrand():
	HAList = []
	for key in ssSegmentDict:
		if key.startswith("B"):
			resnList = ssSegmentDict[key]
			residueList = ssSegmentResidueDict[key]
			if key in ssSegmentHADict:
				HAList = ssSegmentHADict[key]
			
			for resn in resnList:
				if resn in asaDict:
					if asaDict[resn] >= 0.25:
						ss = "Be"
					else: ss = "Bi"
				else:
					ss="B"
				ssDict[resn] = ss
			num1 = 0
			num2 = 0
			for resn in resnList:
				if ssDict[resn] == "Be":
					num1+=1
				elif ssDict[resn] == "Bi":
					num2+=1	
			#print(resnList, len(resnList), num1, num2)
			# calculate the ratio and assign the beta-strand to the the majority class
			#if num1/len(resnList)*100 >= 60:
			ratio1 = num1/(num1+num2)*100
			ratio2 = num2/(num1+num2)*100
			if  ratio1 >= 60:
				#print("Assigning to Be.....")
				
				for resn in resnList:
					ssDict[resn] = "Be"
			elif ratio2 >= 60:
				#print("Assigning to Bi?????")
				ext_strand_flag = check_altering_hydrophilic_hydrophobic(residueList)
				if ext_strand_flag == 0:
					for resn in resnList:
						ssDict[resn] = "Bi"
				else:
					for resn in resnList:
						ssDict[resn] = "Be"
					
			elif ratio1 == 50 or ratio2 == 50:
				# check the alternating Halpha secondary chemical shift pattern or the altenating hydrophilic or hydrophobic residue pattern in surface strand with the exception of two hydrophobic or hydrophilic residues occurence
				
				
				ext_strand_flag = check_altering_hydrophilic_hydrophobic(residueList)
				
				
				
					
						
				#if len(HAList) == len(resnList):
					
					#print ("checking HA sec. shifts", HAList)
					#print(detect_periodicity_sec_cs(HAList))
				if ext_strand_flag == 1:
					
					for resn in resnList:
						ssDict[resn] = "Be"
				else:
					
					for resn in resnList:
						ssDict[resn] = "Bi"
				
def calc_hydropathy_profile():
	pass

	
def calc_ext_int_helix():
	for key in ssSegmentDict:
		if key.startswith("H"):
			resnList = ssSegmentDict[key]
			
			for resn in resnList:
				if resn in asaDict:
					if asaDict[resn] >= 0.25:
						ss = "He"
					else: ss = "Hi"
				else:
					ss="H"
				ssDict[resn] = ss
			num1 = 0
			num2 = 0
			for resn in resnList:
				if ssDict[resn] == "He":
					num1+=1
				elif ssDict[resn] == "Hi":
					num2+=1	
			
			# calculate the ratio and assign the beta-strand to the the majority class
			
			ratio1 = num1/(num1+num2)*100
			ratio2 = num2/(num1+num2)*100
			if  ratio1 >= 50:
				
				for resn in resnList:
					ssDict[resn] = "He"
			elif ratio2 >= 50:
				
				for resn in resnList:
					ssDict[resn] = "Hi"
					
			elif ratio1 == 50 or ratio2 == 50:
				pass
				


def recalc_ss_segments():
	
	num = -1
	segmentStart = 0
	
	for i in range(len(resn_res_array)-1):
		
		current = resn_res_array[i]
		next = resn_res_array[i+1]
		
		if current in ssDict and next not in ssDict:
			if segmentStart:
				ssSegmentResNum[num] += [current]
				segmentStart = 0
			elif not segmentStart:
				num+=1
				ssSegments.append(ssDict[current][0])
				ssSegmentResNum[num] = [current]
		if current in ssDict and next in ssDict:
			
			if not segmentStart:
				num += 1
				
				ssSegments.append(ssDict[current][0])
				
				ssSegmentResNum[num] = [current]
				segmentStart = 1
				if ssDict[current][0] != ssDict[next][0] and segmentStart:
				
					
					segmentStart = 0
				
			elif (ssDict[current][0] == ssDict[next][0] and segmentStart):
				ssSegmentResNum[num] += [current]
			elif (ssDict[current][0] != ssDict[next][0] and segmentStart):
				ssSegmentResNum[num] += [current]
				segmentStart = 0
	
		

def calc_bhairpin(num_segments):
	
	i = 0
	while( i < len(ssSegments)):
		
		
		
		#if i+4 <len(ssSegments) and ((ssSegments[i]== "B" and len(ssSegmentResNum[i]) >= 3)  and ( (ssSegments[i+1]== "T" and ssSegments[i+2] == "C"  and len(ssSegmentResNum[i+2])== 1 and ssSegments[i+3] == "B") or (ssSegments[i+1]== "C" and len(ssSegmentResNum[i+1])== 1 and ssSegments[i+2]== "T" and (ssSegments[i+3]== "B" and len(ssSegmentResNum[i+3]) >= 3)))):
		if i+4 <len(ssSegments) and ((ssSegments[i]== "B" and len(ssSegmentResNum[i]) >= 3)  and ( (ssSegments[i+1]== "T" and ssSegments[i+2] == "C"  and len(ssSegmentResNum[i+2])<= 3 and ssSegments[i+3] == "B") or (ssSegments[i+1]== "C" and len(ssSegmentResNum[i+1])<= 3 and ssSegments[i+2]== "T" and (ssSegments[i+3]== "B" and len(ssSegmentResNum[i+3]) >= 3)))):	
			#print("\nfound beta-hairpin.........\n")
			
			resnList0 = ssSegmentResNum[i]
			resnList0 += ssSegmentResNum[i+1]
			resnList0 += ssSegmentResNum[i+2]
			resnList0 += ssSegmentResNum[i+3]
			
			
		
			
			for resn in resnList0:
				
				ssDict[resn] = ssDict[resn]+" BH"
				
		
			i = i+4
		elif i+3 < len(ssSegments) and ((ssSegments[i]== "B" and len(ssSegmentResNum[i]) >= 3) and ssSegments[i+1]== "T" and (ssSegments[i+2] == "B"and len(ssSegmentResNum[i+2])>=3)):
			resnList0 = ssSegmentResNum[i]
			resnList0 += ssSegmentResNum[i+1]
			resnList0 += ssSegmentResNum[i+2]
			
			for resn in resnList0:
				
				ssDict[resn] = ssDict[resn]+" BH"
			i = i+3	
			
		else:
			resnList = ssSegmentResNum[i]
			for resn in resnList:
				ssDict[resn] = ssDict[resn]+ " " +ssDict[resn] 
			i = i+1

def main():
	read_file()
	num_segments=calc_ss_segments()
	calc_bturn()
	
	calc_ext_int_bstrand()
	#calc_ext_int_helix()
	recalc_ss_segments()
	calc_bhairpin(num_segments)
	
	resnList = []
	AAstring = []
	SSstring = []
	
	
	fout = open(outFile, "w")
	fout.write("#Resn Residue SS SuperSS\n")
	for i in range(len(resn_res_array)):
		key = resn_res_array[i]
		if key in ssDict:
			(resn, residue)= key.split("_")[0], key.split("_")[1]
			resnList.append(int(resn))
			AAstring.append(three2one[residue])
			#(ss, supSS)  = ssDict[key].split()[0], ssDict[key].split()[1]
			#SSstring.append(ss)
			if (len(ssDict[key].split()) < 2): continue
			
			fout.write ("%s %s %s\n" %(resn, residue, ssDict[key]))
	

# main function call here
main()	

