#!/usr/bin/python
import sys

three2one ={'ALA' : 'A','ARG' : 'R','ASN' : 'N','ASP' : 'D','ASX' : 'B','CYS' : 'C','GLN' : 'Q',
	     'GLU' : 'E','GLX' : 'Z','GLY' : 'G','HIS' : 'H','ILE' : 'I','LEU' : 'L','LYS' : 'K',
	     'MET' : 'M','PHE' : 'F','PRO' : 'P','SER' : 'S','THR' : 'T','TRP' : 'W','TYR' : 'Y',
	     'VAL' : 'V','PCA' : 'X'}


inFile = sys.argv[1]


def createSSPredHTMLText():
	CSI_pred="%s.finalpred" % (inFile)
	file_open = open(CSI_pred, "r")
	pred_ss_line = file_open.readlines()
	
		
	pred_ss_line="<br>".join(pred_ss_line)
	file_open.close()
		
	with open(inFile+".sspred.html", "w") as fout:
		fout.write ("<link href='txtstyle.css' rel='stylesheet' type='text/css' /> %s" % pred_ss_line )
	with open("txtstyle.css", "w") as f:
		f.write ("html, body {font-family:Courier}")

def main():
	
	fin = open(inFile+".csi3", "r")
	flines = fin.readlines()
	proteinLen = len(flines)
	SupSSstring = ' '* proteinLen
	SupSSstring = list(SupSSstring)
	resnList = []
	AAstring = []
	SSstring = []
	SShtmlString = []

	bhpinstart = 0
	bturnstart = 0
	bstrandstart = 0
	helixstart = 0
	
	tagged = 0
	index = 0
	for i in range(0, len(flines)-1):
		if flines[i].startswith("#") or len(flines[i].split()) < 4 or len(flines[i+1].split())< 4: continue
		(resn, aa, ss, supss) = flines[i].split()
		(nextresn, nextaa, nextSS, nextSupSS) = flines[i+1].split()
		
		
			
		currentSS = ss
		currentSupSS = supss 
		currentResn = int(resn)
		resnList.append(int(resn))
		AAstring.append(three2one[aa])
		
		if ss == "H" and helixstart == 0 : 
			helixstart = 1
			
			if tagged == 0:
				SShtmlString += ["<font color=\"red\">"]
				tagged = 1
			SShtmlString+=["H"]	
		elif helixstart == 1 and currentSS == nextSS:		
			SShtmlString+=["H"]	
		elif helixstart == 1 and currentSS != nextSS:
			SShtmlString+=["H"]
			helixstart = 0
			tagged = 0
			SShtmlString+=["</font>"]
		if ss == "C" and tagged == 0:
			SShtmlString+=["C"]
		if ss.startswith("B") and bstrandstart == 0 : 
			bstrandstart = 1
			if ss[-1] == "e" and tagged == 0:
				SShtmlString += ["<font color=\"blue\">"]
				tagged = 1
			elif ss[-1] == "i" and tagged == 0:
				SShtmlString += ["<font color=\"cyan\">"]
				tagged = 1
			SShtmlString+=["B"]	
		elif bstrandstart == 1 and currentSS == nextSS:		
			SShtmlString+=["B"]
		elif bstrandstart == 1 and currentSS != nextSS:
			SShtmlString+=["B"]
			bstrandstart = 0
			tagged = 0
			SShtmlString+=["</font>"]
		if ss.startswith("T") and bturnstart == 0 : 
			bturnstart = 1
			if ss[-1] == "1" and tagged == 0:
				SShtmlString += ["<font color=\"magenta\">"]
				tagged = 1
			elif ss[-1] == "2" and tagged == 0:
				SShtmlString += ["<font color=\"green\">"]
				tagged = 1	
			elif ss[-2:] == "1p" and tagged == 0:
				SShtmlString += ["<font color=\"crimson\">"]
				tagged = 1	
			elif ss[-2:] == "2p" and tagged == 0:
				SShtmlString += ["<font color=\"yellow\">"]
				tagged = 1	
			elif ss[-1] == "8" and tagged == 0:
				SShtmlString += ["<font color=\"brown\">"]
				tagged = 1
			SShtmlString+=["T"]	
		elif bturnstart == 1 and currentSS == nextSS:		
			SShtmlString+=["T"]
		elif bturnstart == 1 and currentSS != nextSS:
			SShtmlString+=["T"]
			bturnstart = 0
			tagged = 0
			SShtmlString+=["</font>"]
			
		
		
		
		if ss == "Be" or ss == "Bi":#blue, cyan	
			ss = "B"
		elif ss == "T1" or ss == "T2" or ss == "T1p" or ss == "T2p" or ss == "T8":#Magenta, Green, Brown, Crimson/Gray, Yellow
			ss = "T"
		elif ss == "H": #Red
			ss = "H"
		else:
			ss = "C" #Black
		SSstring.append(ss)	
		
		if bhpinstart == 0 and supss.startswith("BH") :
			bhpinstart = 1
			start = index
		if bhpinstart == 1 and currentSupSS != nextSupSS:
			bhpinstart = 0
			end = index+1
			bhpinlen = end - start + 1
			if bhpinlen >= 15:
				SupSSstring [start:start+3] = "|--"
				SupSSstring [start+4:start+13] = "B-hairpin"
				SupSSstring [end-3: end+1] = "--|"
                        else:
				SupSSstring [start:start+3] = "|--"
                                SupSSstring [start+4:start+13] = "B-hpin"
                                SupSSstring [end-3: end+1] = "--|"			
			#supss = "beta-hairpin"
		
		#prevSS = currentSS
		#prevSupSS = currentSupSS
		index += 1
		
		# appending the last residue information
		if i+1 == len(flines)-1:
			SShtmlString+=[nextSS]
			SSstring+=[nextSS]
			resnList+=[int(nextresn)]
			AAstring+=[three2one[nextaa]]
			SupSSstring +=" "
			
	if tagged == 1:
			SShtmlString+=["</font>"]		
	#print("".join(SShtmlString))
	
	
	output = ''
	proteinLen = len(resnList)
	j = 0
	k = 0
	for i in range(0,len(resnList), 50):
		SShtml = ""	
		SupSShtml = ""
		
		if i >= 50: SShtml+=font_color# to start the font tag at the beginning of each 50 residue line
		start = i        
		end = start + 50-1
		if end > proteinLen:            
			end = proteinLen-1
		#print(end)	

		
		count = 0
		while (count <= 49 and j < len(SShtmlString)):
			
			if SShtmlString[j].startswith("<"):
				if SShtmlString[j].startswith("<font color"):
					font_color= SShtmlString[j]
                                elif SShtmlString[j].startswith("</font>"):
                                        font_color='' 
				SShtml+= SShtmlString[j]
				
			else:	
				SShtml+= SShtmlString[j]
				count+=1
		
			j+=1
			
		SShtml+="</font>" # to end the font tag in html after each 50 residue 	

		count = 0
		while (count <= 49 and k < len(SupSSstring)):
			
			if SupSSstring[k]==" ":
				SupSShtml+= "&nbsp;"
				
			else:	
				SupSShtml+= SupSSstring[k]
			count+=1
			k+=1	
		#print(count)	
		output+="%d\t%s\t%d\n" %(resnList[start], "".join(AAstring[start:end+1]), resnList[end])
		#output+="%d\t%s\t%d\n" %(resnList[start], "".join(SSstring[start:end+1]), resnList[end])
		output+="%d\t%s\t%d\n" %(resnList[start], SShtml, resnList[end])
		output+="%d\t%s\t%d\n" %(resnList[start], SupSShtml, resnList[end])
		output+='\n'
	#print(output)	
	finalpredfile = inFile+'.finalpred'
	with open(finalpredfile, "w") as fout:
		fout.write(output)
	createSSPredHTMLText()	
	
main()	
