#!/usr/bin/python

from __future__ import division
import os.path
import operator
import sys
#==================================================
# This python script takes a talosplus predicted
# torsion angle and identifies different types of
# (type I, type Ip, type II, type IIp) beta-turns
# and classifies them, identifies external and
# internal beta-strands based on accessible surface 
# area class and finally locates the beta-hairpins
#===================================================

#Global variables 
resmap = {'A' : 'ALA','R' : 'ARG','N' : 'ASN','D' : 'ASP','B' : 'ASX','C' : 'CYS', 'a':'CYS', 'b':'CYS', 'c':'CYS', 'Q' : 'GLN','E' : 'GLU','Z' : 'GLX','G' : 'GLY','H' : 'HIS','I' : 'ILE','L' : 'LEU','K' : 'LYS','M' : 'MET','F' : 'PHE','P' : 'PRO','S' : 'SER','T' : 'THR','W' : 'TRP','Y' : 'TYR','V' : 'VAL','X' : 'PCA'}

three2one ={'ALA' : 'A','ARG' : 'R','ASN' : 'N','ASP' : 'D','ASX' : 'B','CYS' : 'C','GLN' : 'Q','GLU' : 'E','GLX' : 'Z','GLY' : 'G','HIS' : 'H','ILE' : 'I','LEU' : 'L','LYS' : 'K','MET' : 'M','PHE' : 'F','PRO' : 'P','SER' : 'S','THR' : 'T','TRP' : 'W','TYR' : 'Y','VAL' : 'V','PCA' : 'X'}

bturnAAPositionalPreference={'I': [['D','N','C','S', 'P', 'H'], ['P', 'E', 'S'], ['D', 'N', 'S', 'T'], ['G']], 'II': [['P', 'Y'], ['K', 'P'], ['G', 'N'], ['S', 'K', 'C']], 'VIII': [['P', 'G'], ['P', 'D'], [ 'N', 'D', 'V'], ['P', 'T']], 'Ip': [['Y', 'I', 'V'], ['N', 'D','G','H'], [ 'G'], ['K']], 'IIp': [['Y'], ['G'], [ 'N', 'D', 'S'], ['G', 'T']] }

#bturnAllPreferredRes = ['D', 'N', 'H', 'P', 'S', 'E', 'T', 'C', 'Y','K', 'I', 'V', 'G']

#Reference: "A revised set of beta-turn potentials", Hutchinson et el. 1994
bturntypePreferredRes={"T1": {"I":[0.32, 0.6, 0.29, 0.75], "F":[0.81, 0.42, 1.13, 1.01], "V": [0.39, 0.72, 0.50, 0.77], "L":[0.68,0.70, 0.52, 0.74], "W":[0.54, 0.65, 1.30, 1.14], "M":[0.65, 0.53, 0.41, 0.61], "A": [0.63, 1.07, 0.80, 0.87], "G":[1.07, 0.40, 0.61, 2.38], "C":[1.57, 0.87, 0.96, 1.35], "Y":[0.66, 0.61, 0.90,0.85], "P": [1.31, 3.49, 0.21, 0.03], "T":[1.11, 0.94, 1.44, 1.11], "S":[1.52, 1.50, 1.29, 1.02], "H":[1.60, 0.53, 0.99, 0.92], "E":[0.74, 1.54,1.21, 1.02], "N":[2.25, 1.54, 1.21, 1.02], "Q":[0.72, 0.81, 1.18, 1.08], "D":[2.51, 1.20, 2.54, 1.11], "K":[0.70, 1.20, 1.01, 1.10],"R":[0.64, 0.86, 0.88, 0.78]}, 
                       
"T2":{"I": [1.01, 0.64, 0, 1.10], "F":[1.23, 0.74, 0.37, 0.80], "V": [1.12, 0.43, 0.0, 1.12], "L":[0.73,0.70, 0.21, 0.70], "W":[0.49, 0.66, 0.33, 0.99], "M":[0.74, 0.62, 0.12, 1.36], "A": [1.05, 1.22, 0.14, 1.07], "G":[0.96, 0.18, 9.17, 0.57], "C":[0.13, 0.0, 0.26, 1.72], "Y":[1.59, 0.73, 0.20,1.26], "P": [1.99, 4.91, 0.0, 0.0], "T":[0.91, 0.67, 0.12, 1.12], "S":[0.88, 0.96, 0.31, 1.38], "H":[1.51, 0.97, 0.65, 0.97], "E":[1.0, 1.21,0.13, 1.21], "N":[1.15, 0.65, 1.25, 0.55], "Q":[1.19, 1.26, 0.56, 1.40], "D":[0.34, 1.02, 0.51, 0.89], "K":[1.07, 1.41, 0.26, 1.58],"R":[0.70, 0.81, 0.16, 1.03]},

"T1p":{"I": [1.61, 0.15, 0.0, 0.74], "F":[1.18, 0.59, 0.78, 0.40], "V": [1.50, 0.0, 0.0, 1.28], "L":[0.87, 0.29, 0.0, 0.68], "W":[1.05, 0.0, 1.05, 0.53], "M":[0.39, 0.80, 0.39, 1.19], "A": [0.99, 0.82, 0.09, 0.64], "G":[0.48, 1.92, 8.66, 0.38], "C":[1.27, 0.43, 0, 0], "Y":[2.54, 0.43, 1.27, 1.28], "P": [0.50, 0, 0, 0], "T":[1.06, 0, 0.13, 0.67], "S":[1.10, 0.99, 0.37, 0.62], "H":[2.06, 4.16, 0.69, 0], "E":[0.40, 0.67, 0.4, 1.61], "N":[0.35, 5.26, 0.7, 1.75], "Q":[0.45, 0.68, 0.22, 1.58], "D":[0.94, 2.58, 0.54, 0.82], "K":[0.86, 0.70, 0.17, 1.74],"R":[0.86, 0.7, 0.17, 1.74]}, 
"T2p":{"I": [0.42, 0.21, 0.62, 0], "F":[1.40, 0.28, 0.84, 1.38], "V": [1.64, 0, 0.33, 0.81], "L":[0.55, 0, 0.55, 0.54], "W":[0, 0, 0.75, 0.74], "M":[0, 0, 0.56, 0.56], "A": [0.77, 0.51, 1.16, 1.15], "G":[1.09, 9.24, 1.09, 1.75], "C":[1.20, 0.60, 0, 1.19], "Y":[2.42, 0.0, 0.30, 1.20], "P": [0.48, 0, 1.19, 0], "T":[0.95, 0, 0.38, 2.25], "S":[1.57, 0.70, 2.10, 1.21], "H":[1.47, 0, 1.47, 0], "E":[1.33, 0.19, 0.95, 0.56], "N":[0.99, 0.25, 2.48, 1.47], "Q":[1.28, 0.32, 0.64, 0.95], "D":[1.16, 0.77, 1.93, 0.38], "K":[0.19, 0.39, 1.17, 1.35],"R":[0.74, 0.25, 0.49, 1.46]},
"T8":{"I": [0.92,0.97,1.26,0.63], "F":[0.69, 0.31, 1.54, 1.0], "V": [0.77, 0.99, 1.58, 1.31], "L":[0.45, 0.87, 1.06, 0.83], "W":[1.24, 0.21, 0.82, 0.21], "M":[0.46, 0.46, 0.62, 0.31], "A": [0.88, 1.06, 0.53, 0.89], "G":[1.49, 0.30, 0.15, 0.67], "C":[1.16, 0.99, 0.99, 0.33], "Y":[0.91, 0.50, 1.50, 0.67], "P": [2.74, 2.09, 0.0, 4.06], "T":[1.14, 0.99, 1.30, 1.46], "S":[1.39, 1.15, 0.86, 1.01], "H":[0.81, 0.40, 1.35, 0.81], "E":[0.89, 1.25, 0.52, 0.52], "N":[1.30, 0.82, 1.91, 0.89], "Q":[0.79, 1.23, 0.79, 0.79], "D":[0.63, 1.96, 1.75, 0.80], "K":[0.53, 1.34, 1.07, 0.91],"R":[0.81, 0.95, 1.02, 0.75]}}

#"T2":{0:["P","Y"],1:["P","K"], 2:["G","N"], 3:["S","K", "C"]}, "T1p":{0:["Y", "I", "V"],1:["N","D", "G"], 2:["G"], 3:["K"]}, "T2p":{0:["Y"], 1:["G"], 2:["N", "S", "D"], 3:["T", "G"]}, "T8":{0:["P", "G"], 1:["P", "D"], 2:["N","D", "V"],3:["P", "T"]}

betaHelixPhiPsi = {"B": [-130, +140], "H": [-65, -40]}

# [polar, special (non-polar), non-polar]
# polar side cahin contain groups that are either charged at physiological pH or groups that are able to partcipate in hydrogen bonding.
# non-polar side chains consist mainly of hydrocarbon. Any fuctinal group they contain are uncharged at physiological pH and are incapable of participating in hydrogen bonding

residue_groups_by_polarity = [['K', 'R', 'H', 'E', 'D', 'S', 'N', 'Q', 'T', 'Y'], ['C', 'G', 'P','F', 'M', 'I', 'L','V', 'W', 'A']]
chargedResidueList = ['R', 'K', 'H', 'D', 'E']


# variable initialization
inFile = sys.argv[1]

angleFile = inFile+".anglepred"
ssFile = inFile+".sspred1"
asaFile = inFile+".asapred"
tabFile = inFile+".tab"
outFile = inFile+".csi3"
s2File = inFile+".str.S2.txt"


ssDict = {}
ssDictOrig={}
phiDict = {}
psiDict = {}
asaDict = {}
HAsecshiftDict = {}
HNsecshiftDict = {}
CAsecshiftDict = {}
ssSegmentDict = {}
s2Dict ={}
ssSegmentResidueDict = {}
ssSegmentHADict = {}
ssSegmentHNDict = {}
ssSegments = []
ssSegmentResNum = {}
resn_res_array=[]
keyList = []
bturnDict={}
bstrandDict={}
bhpinDict={}


def IN (angle, target):
	a1 = angle
	a2 = target
	while(a1 < -180):
		a1 +=360
	while(a1 >= 180):
		a1 -=360
	while(a2 < -180):
		a2 +=360
	while(a2>=180):
		a2 -=360
	if(a1-a2) >= 0:
		if a1-a2 < 180:
			pass
		else:
			a2+=360
	else:
		if a1-a2 > -180:
			pass
		else:
			a1+=360
	return abs(a1-a2)		
def isSubSet (List, subList):
	count = 0
	for elem in subList:
		if elem not in List:
			count +=1 
			#return False
	#return True
	# return true if ateast 3 out 4 residues are preferred for b-turn conformation
	if count == 0:
		return True
	else:
		return False


def bturnResidueCheck(residueList, turnAssigned):
	residue_potential = 0
	if turnAssigned == "T1":
		for k in range (len(residueList)):
			
			residue_potential+=bturntypePreferredRes["T1"][residueList[k]][k]
		#print(residueList, residue_potential, "????????")	
		if residue_potential > 2.5:
			return True
	else:
		for k in range (len(residueList)):
			
			residue_potential+=bturntypePreferredRes[turnAssigned][residueList[k]][k]
		print(residueList, turnAssigned,residue_potential, ".........")		
		if residue_potential > 4.0:
			return True		
	return False		
	
def read_file():
        global s2File, ssFile
        
	fss = open(ssFile, "r")
        fss = list(fss)
        #print(list(fss))       
        if not fss: # if *.sspred1 is empty, try reading the *.sspred file 
            ssFile = inFile+ ".sspred"
            fss = open(ssFile, "r")  
	fangle = open(angleFile, "r")
	fasa = open(asaFile, "r")
        if not os.path.exists(s2File):
                s2File = inFile+".str_BMRB.S2.txt"	
        fs2 = open (s2File, "r")

	
	# reading secondary chemical shift infromation
	if os.path.exists(tabFile):
		ftab = open(tabFile, "r")
		for line in ftab:
			if line[0].isdigit():
				if len(line.split()) < 6: continue
				(resn, residue, atom, d,d, seccsshift) = line.split()
				if atom == "HA":
					HAsecshiftDict[resn+'_'+residue] = round(float(seccsshift),2)
				if atom == "HN":
					
					HNsecshiftDict[resn+'_'+residue] = round(float(seccsshift),2)
				if atom == "CA":
					
					CAsecshiftDict[resn+'_'+residue] = round(float(seccsshift),2)	
					
	for line in fss:
                print(line)
		if line.strip() and not line.startswith("#"):
		        (resn, residue, ss) = line.split()
			ssDict[resn+'_'+resmap[residue]] = ss
			ssDictOrig[resn+'_'+resmap[residue]] = ss
	for line in fs2:
		(resn, s2order, residue) = line.split()
		s2Dict[resn+"_"+resmap[residue]] = float(s2order)
                #resn_res_array.append(resn+'_'+resmap[residue])
			
	for line in fasa:
		if not line.startswith("#"):
			(resn, residue, asa) = line.split()#.asapred extension
			#(d, resn, residue, d, asa) = line.split()#.asapred2 extension
			asaDict[resn+'_'+residue] = round(float(asa), 2) 		
	for line in fangle:
		line= line.strip()
		if line.strip() and line[0].isdigit():
			elems = line.split()
			resn = elems[0]
			residue = elems[1]
			phi = round(float(elems[2]),1)
			psi = round(float(elems[3]),1)
			
			resn_res_array.append(resn+'_'+resmap[residue])
			
			#if float(phi) == 9999.000 or float(psi) == 9999.000:
			#	continue
			phiDict[resn+'_'+resmap[residue]] = phi
			psiDict[resn+'_'+resmap[residue]] = psi 
			#print("%s %s %0.1f,%0.1f"%(resn, resmap[residue], phi, psi))
def check_extend_shrink_ss_boundaries():
	for key in ssSegmentDict:
		if key.startswith("B") or key.startswith("H"):
			currentSS = key.split(".")[0]
			if currentSS == "B": 
				phiDiffAllowed = 50
				psiDiffAllowed = 40
			elif currentSS == "H": 
				phiDiffAllowed = 20
				psiDiffAllowed = 30
			next_key = ''
			current_key_index  = keyList.index(key)
			prev_key= keyList[current_key_index-1]
			if  current_key_index+1 < len(keyList)  :
				
				next_key = keyList[current_key_index+1]
				nextSS = next_key.split(".")[0]
			# start and end locations check for boundary extension
			
			prevSS = prev_key.split(".")[0]
			
			prevResn= ssSegmentDict[prev_key][-1] 
			startResn = ssSegmentDict[key][0]
			endResn = ssSegmentDict[key][-1]
			
			
			if next_key in ssSegmentDict:
				nextResn = ssSegmentDict[next_key][0]
			
			#print (prevResn, startResn, endResn, nextResn)	
			
			ideal_phi = betaHelixPhiPsi[currentSS][0]
			ideal_psi = betaHelixPhiPsi[currentSS][1]
			
			if (IN(phiDict[startResn], ideal_phi) <= phiDiffAllowed and IN(psiDict[startResn], ideal_psi) <= psiDiffAllowed and s2Dict[startResn] > 0.7):
						
				if (IN(phiDict[prevResn], ideal_phi) <= phiDiffAllowed and IN(psiDict[prevResn], ideal_psi) <= psiDiffAllowed and s2Dict[prevResn] > 0.7):
					print(prevResn, phiDict[prevResn], psiDict[prevResn], currentSS, "extending")
					ssDict[prevResn] = currentSS
					
					
			else:
				#print(endResn, phiDict[startResn], psiDict[startResn], currentSS, "shrinking")
				ssDict[startResn] = prevSS
			
			
			if (IN(phiDict[endResn], ideal_phi) <= phiDiffAllowed and IN(psiDict[endResn], ideal_psi) <= psiDiffAllowed and s2Dict[endResn] > 0.7):
				if (IN(phiDict[nextResn], ideal_phi) <= phiDiffAllowed and IN(psiDict[nextResn], ideal_psi) <= psiDiffAllowed and s2Dict[nextResn] > 0.7):
					#print(nextResn, phiDict[nextResn], psiDict[nextResn], currentSS, "extending")
					ssDict[nextResn] = currentSS
					
					
			else:
				#print(endResn, phiDict[endResn], psiDict[endResn], currentSS, "shrinking")
				ssDict[endResn] = nextSS
				
			
				## AH and cA shift checking in helix and beta sheet regions 
				#shift_score = 0
				#if currentSS == "B":
					#if resn in HAsecshiftDict:
						#if HAsecshiftDict[resn] > 0:
							#shift_score+=1
					#if resn in CAsecshiftDict:		
						#if CAsecshiftDict[resn] < 0:
							#shift_score+=1	
				#if currentSS == "H":
					#if resn in HAsecshiftDict:
						#if HAsecshiftDict[resn] < 0:
							#shift_score+=1
					#if resn in CAsecshiftDict:		
						#if CAsecshiftDict[resn] > 0:
							#shift_score+=1
				
					
					
						
			

def calc_ss_segments():
	num = 0
	segmentStart = 0
	
	# checking the RCI S2 order parameter and change to coil assignemnt if it is <=0.70
	
	for resn in ssDict:
                #print(resn, ssDict[resn], "...........") 
		if resn in s2Dict:
			if s2Dict[resn] <= 0.7:
				ssDict[resn] = "C"
	
	for i in range(len(resn_res_array)-1):
		current = resn_res_array[i]
		current_res = three2one [current.split("_")[1]]
		next = resn_res_array[i+1]
		
		
		if current in ssDict and next not in ssDict:
			if segmentStart:
				ssSegmentDict[ssDict[current]+"."+str(num)] += [current]
				ssSegmentResidueDict[ssDict[current]+"."+str(num)] += [current_res]
				if current in HAsecshiftDict:
					ssSegmentHADict[ssDict[current]+"."+str(num)] += [HAsecshiftDict[current]]
				else:
					ssSegmentHADict[ssDict[current]+"."+str(num)] += [-999]
				segmentStart = 0
			elif not segmentStart:
				num+=1
				ssSegmentDict[ssDict[current]+"."+str(num)] = [current]
				keyList.append(ssDict[current]+"."+str(num))
				ssSegmentResidueDict[ssDict[current]+"."+str(num)] = [current_res]
				if current in HAsecshiftDict:
					ssSegmentHADict[ssDict[current]+"."+str(num)] = [HAsecshiftDict[current]]
				else: ssSegmentHADict[ssDict[current]+"."+str(num)] = [-999]	
		
		if current in ssDict and next in ssDict:
			 
			
			if not segmentStart:
				num += 1
				keyList.append(ssDict[current]+"."+str(num))
				ssSegmentDict[ssDict[current]+"."+str(num)] = [current]
				ssSegmentResidueDict[ssDict[current]+"."+str(num)] = [current_res]
				if current in HAsecshiftDict:
					ssSegmentHADict[ssDict[current]+"."+str(num)] = [HAsecshiftDict[current]]
				else:ssSegmentHADict[ssDict[current]+"."+str(num)] = [-999]
				
				segmentStart = 1 
				# if a ss segment is of 1 residue length 
				if ssDict[current] != ssDict[next] and segmentStart:
				
					ssSegmentDict[ssDict[current]+"."+str(num)] += [current]
					ssSegmentResidueDict[ssDict[current]+"."+str(num)] += [current_res]
					if current in HAsecshiftDict:
						ssSegmentHADict[ssDict[current]+"."+str(num)] += [HAsecshiftDict[current]]
					else: ssSegmentHADict[ssDict[current]+"."+str(num)] += [-999]
					segmentStart = 0
			elif ssDict[current] == ssDict[next] and (segmentStart==1):
				ssSegmentDict[ssDict[current]+"."+str(num)] += [current]
				
				
				ssSegmentResidueDict[ssDict[current]+"."+str(num)] += [current_res]
				if current in HAsecshiftDict:
					ssSegmentHADict[ssDict[current]+"."+str(num)] += [HAsecshiftDict[current]]
				else: ssSegmentHADict[ssDict[current]+"."+str(num)] += [-999]
				
			elif ssDict[current] != ssDict[next] and segmentStart:
				
				ssSegmentDict[ssDict[current]+"."+str(num)] += [current]
				ssSegmentResidueDict[ssDict[current]+"."+str(num)] += [current_res]
				if current in HAsecshiftDict:
					ssSegmentHADict[ssDict[current]+"."+str(num)] += [HAsecshiftDict[current]]
				else:ssSegmentHADict[ssDict[current]+"."+str(num)] += [-999]	
				
				segmentStart = 0
	return num			
	 	
def calc_bturn():
	
	
	for key in ssSegmentDict:
		bturn = 0
		resnList = []
		residueList=[]
		if key.startswith("C"):
			next_key = ''
			current_key_index  = keyList.index(key)
			prev_key= keyList[current_key_index-1]
			if  current_key_index+1 < len(keyList)  :
				
				next_key = keyList[current_key_index+1]
			
				
			if prev_key.startswith("B") and not next_key.startswith("B"): 	
				resnList += [ssSegmentDict[prev_key][-1]]
				resnList+=ssSegmentDict[key]
				residueList += [ssSegmentResidueDict[prev_key][-1]]
				residueList+= ssSegmentResidueDict[key]
				
			elif not prev_key.startswith("B") and next_key.startswith("B"):
				resnList+=ssSegmentDict[key]
				residueList+= ssSegmentResidueDict[key]
				if next_key in ssSegmentDict:
					resnList+=[ssSegmentDict[next_key][0]]
					residueList+=[ssSegmentResidueDict[next_key][0]]
			elif prev_key.startswith("B") and next_key.startswith("B"): 	
				resnList += [ssSegmentDict[prev_key][-1]]
				resnList+=ssSegmentDict[key]
				residueList += [ssSegmentResidueDict[prev_key][-1]]
				residueList+= ssSegmentResidueDict[key]	
				if next_key in ssSegmentDict:
					resnList+=[ssSegmentDict[next_key][0]]
					residueList+=[ssSegmentResidueDict[next_key][0]]
					
			else:
				resnList = ssSegmentDict[key]
				residueList = ssSegmentResidueDict[key]
				
			#print(resnList)
			
			
			if len(resnList) > 4:
				i = 0
				while( i < len(resnList)-3):
					num_flexible_res = 0
					middle_res_flexible = 0
					resnL = resnList[i:i+4]
					residueL = residueList[i:i+4]
					#print(resnL)
					for n in range(len( resnL)):
                                                s2order = None
						if resnL[n] in s2Dict:
							s2order= s2Dict[resnL[n]]
						else:
							if n+1 < len(resnL) and resnL[n-1] in s2Dict and resnL[n+1] in s2Dict:
								s2order = (s2Dict[resnL[n-1]]+s2Dict[resnL[n+1]])/2
							#elif  n+1 == len(resnL) or (resnL[n-1] in s2Dict and not resnL[n+1] in s2Dict):
								#s2order = (s2Dict[resnL[n-1]]+s2Dict[resnL[n-2]])/2
									
	
						#print(resnL[n], s2order)
                                                
								
						if s2order != None and s2order  <= 0.70  :
							num_flexible_res += 1
							if (n== 1 or n==2):
								middle_res_flexible  = 1 
							
					if num_flexible_res >= 2 or middle_res_flexible :
						i = i+4
						continue
					
					phi1, psi1 = phiDict[resnL[1]], psiDict[resnL[1]]
					phi2, psi2 = phiDict[resnL[2]], psiDict[resnL[2]]
					#angleL = [phiDict[resnL[1]], psiDict[resnL[1]], phiDict[resnL[2]], psiDict[resnL[2]]]
					
					if residueL[1] == "G" or residueL[2] == "G":
						bounds=[]
						bounds += [40.0]
						bounds += [40.0]
						bounds += [35.0]
						bounds += [35.0]
					else:
						bounds = []
						bounds += [30.0]
						bounds += [30.0]
						bounds += [30.0]
						bounds += [30.0]
					
					#type-I check
					
					
					if IN (phi1, -60) <= bounds[0] and IN (psi1, -30) <= bounds[1] and IN(phi2, -90) <= bounds[2] and IN (psi2, 0) <= bounds[3]:
					
						# I
						
						#print(phi1, psi1, phi2, psi2, resnL)
						turnAssigned  = "T1"
						bturn = 1
							
					#type-I' check		
					
					if IN (phi1, 60) <= bounds[0] and IN (psi1, 30) <= bounds[1] and IN(phi2, 90) <= bounds[2] and IN (psi2, 0) <= bounds[3]:
						# I'
						turnAssigned =  "T1p"
						bturn = 1
						
					#type-II check	
					
					if IN (phi1, -60) <= bounds[0] and IN (psi1, 120) <= bounds[1] and IN(phi2, 80) <= bounds[2] and IN (psi2, 0) <= bounds[3]:
					
						#II
						turnAssigned=  "T2"
						bturn = 1
						
					#type-II' check	
					
					if IN (phi1, 60) <= bounds[0] and IN (psi1, -120) <= bounds[1] and IN (phi2, -80) <= bounds[2] and IN (psi2, 0) <= bounds[3]:
						# II'
						turnAssigned = "T2p"
						bturn = 1
						
					#type-VIII check	
					
					if IN (phi1, -60) <= bounds[0] and IN (psi1, -30) <= bounds[1] and IN(phi2, -120) <= bounds[2] and IN (psi2, 120) <= bounds[3]:
						# VIII
						turnAssigned  = "T8"
						bturn = 1
						
				
					if bturn and bturnResidueCheck(residueL, turnAssigned) == True:
						#print("Assigning type " , turnAssigned ," to residue.......", resnL)
						for j in range (len(resnL)):
							bturnDict[resnL[j]] = turnAssigned
							ssDict[resnL[j]] = turnAssigned
						i = i+4
						
					else:
						i = i+1
					bturn = 0	
			
			elif len(resnList) > 3 and len(resnList) <= 4 :
				num_flexible_res = 0
				middle_res_flexible = 0
				for n in range(len( resnList)):
                                        s2order = None
					if resnList[n] in s2Dict:
						s2order= s2Dict[resnList[n]]
					else:
						if n+1 < len(resnList) and resnList[n-1] in s2Dict and resnList[n+1] in s2Dict:
							s2order = (s2Dict[resnList[n-1]]+s2Dict[resnList[n+1]])/2
						#elif  n+1 == len(resnList) or ( (resnList[n-2] in s2Dict and resnList[n-1] in s2Dict) and not resnList[n+1] in s2Dict):
						#	s2order = (s2Dict[resnList[n-1]]+s2Dict[resnList[n-2]])/2
					#print(resnList[n], s2order, "........")	
					if s2order!= None and s2order  <= 0.70:
							num_flexible_res += 1
							if (n== 1 or n==2):
								middle_res_flexible  = 1 
				if num_flexible_res >= 2 or middle_res_flexible:
					continue
				
				phi1, psi1 = phiDict[resnList[1]], psiDict[resnList[1]]
				phi2, psi2 = phiDict[resnList[2]], psiDict[resnList[2]]
				
				
				
				if residueList[1] == "G" or residueList[2] == "G":
					bounds=[]
					bounds += [40.0]
					bounds += [40.0]
					bounds += [35.0]
					bounds += [35.0]
				else:
					bounds=[]
					bounds += [30.0]
					bounds += [30.0]
					bounds += [30.0]
					bounds += [30.0]
				
				if IN (phi1, -60) <= bounds[0] and IN (psi1, -30) <= bounds[1] and IN(phi2, -90) <= bounds[2] and IN (psi2, 0) <= bounds[3]:	
						# I
						turnAssigned  = "T1"
						bturn = 1
							
							
				
				if IN (phi1, 60) <= bounds[0] and IN (psi1, 30) <= bounds[1] and IN(phi2, 90) <= bounds[2] and IN (psi2, 0) <= bounds[3]:
						# I'
						turnAssigned =  "T1p"
						bturn = 1
						
				
				if IN (phi1, -60) <= bounds[0] and IN (psi1, 120) <= bounds[1] and IN(phi2, 80) <= bounds[2] and IN (psi2, 0) <= bounds[3]:
						#II
						turnAssigned=  "T2"
						bturn = 1
						
				
				if IN (phi1, 60) <= bounds[0] and IN (psi1, -120) <= bounds[1] and IN (phi2, -80) <= bounds[2] and IN (psi2, 0) <= bounds[3]:
						# II'
						turnAssigned = "T2p"
						bturn = 1
						
				
				if IN (phi1, -60) <= bounds[0] and IN (psi1, -30) <= bounds[1] and IN(phi2, -120) <= bounds[2] and IN (psi2, 120) <= bounds[3]:
						# VIII
						turnAssigned  = "T8"
						bturn = 1
					
				if bturn and bturnResidueCheck(residueList, turnAssigned) == True:
					#print("Assigning type " , turnAssigned ," to residue.......", resnList)
					for j in range( len(resnList)):
						bturnDict[resnList[j]] = turnAssigned
						ssDict[resnList[j]] = turnAssigned
					bturn = 0	

def detect_periodicity_sec_cs(secshiftList):
	periodicity = 0
	
	for i in range (1,len(secshiftList)-1, 2):
		
		prev_shift = secshiftList[i-1]
		current_shift = secshiftList[i]
		next_shift = secshiftList[i+1]
		if (current_shift < prev_shift and next_shift > current_shift) or (current_shift > prev_shift and next_shift < current_shift):
			periodicity = 1
		else:
			periodicity = 0
			break
		
	return periodicity	
		
def check_altering_hydrophilic_hydrophobic(residueList):
	
	
	residue_polarity = []
	ext_strand_flag = 0
	for residue in residueList:
		if residue in residue_groups_by_polarity[0]:
			residue_polarity += ["polar"]
		elif residue in residue_groups_by_polarity[1]:
			residue_polarity+=["apolar"]
			
	for i in range( 1, len(residue_polarity)-1, 1):
		prev = residue_polarity[i-1]
		current = residue_polarity[i]
		next = residue_polarity[i+1]
		
		#print(prev, current, next)
		if (prev == "polar" and current == "apolar" and next == "polar") or (prev == "apolar" and current == "polar" and next == "apolar"):
			ext_strand_flag = 1
	
		elif (prev == "polar" and current == "apolar" and next == "apolar") or (prev == "apolar" and current == "polar" and next == "polar") or (prev == "polar" and current == "polar" and next == "apolar") or (prev == "apolar" and current == "apolar" and next == "polar") :
			ext_strand_flag = 1
		elif (prev == "apolar" and current == "apolar" and next == "apolar") or (prev == "polar" and current == "polar" and next == "polar"):
			ext_strand_flag = 0
			break
	return ext_strand_flag 		
	
def periodicity_polarity(residueList):
	residue_polarity_swap_count = 0
	for i in range( 0, len(residueList)-1):
		if residueList[i] in residue_groups_by_polarity[0] and residueList[i+1] in residue_groups_by_polarity[1] :
			residue_polarity_swap_count += 1
		else:
			residue_polarity_swap_count += 0
			
	return 	(residue_polarity_swap_count/ len(residueList))	

def calc_exposed_ratio(resnList):
	num = 0
	for resn in resnList:
		if resn in asaDict:
			
			if float(asaDict[resn]) >= 0.24:
				num+=1
	ratio = num/(len(resnList))*100
	#print(resnList, "has ratio:", ratio)
	return ratio
def calc_fraction_high_s2_residues(resnList):
	for resn in resnList:
		if resn in s2Dict:
			
			if float(s2Dict[resn]) >= 0.90:
				num+=1
	percent_fraction = num/(len(resnList))*100
	#print(resnList, "has ratio:", ratio)
	return percent_fraction
	

def calc_edge_internal_strand():
	num_bstrands = 0
	num_edge_strands = 0
	bstrands_dict = {}
	for key in ssSegmentDict:
		if key.startswith("B"):
			resnList = ssSegmentDict[key]
			residueList = ssSegmentResidueDict[key]
			bstrands_dict[key] = [resnList, residueList] 
			num_bstrands += 1
	if num_bstrands == 2:
		num_edge_strands+=2
		for key in bstrands_dict:
			resnList= bstrands_dict[key][0]
			for resn in resnList:
				
				bstrandDict[resn] = "E"
				
				
	if num_bstrands >= 3:
		
		weight_dict = {}
		
		for key in bstrands_dict:
			weight = 0
			resnList, residueList = bstrands_dict[key]
			#print(resnList, residueList)
			if key in ssSegmentHADict:
				HAList = ssSegmentHADict[key]
				#print(HAList)
			if calc_exposed_ratio(resnList) >=50:
				#print(key, "has more than 50% exposed residues")
				weight+=0.5
			if calc_fraction_high_s2_residues <= 40:
				weight+=0.5
			if( check_altering_hydrophilic_hydrophobic(residueList)==1):
				weight+=0.5
				
			if detect_periodicity_sec_cs(HAList):
				weight+=0.5
			total_charge = 0	
			for res in residueList:
				if res in chargedResidueList:
					total_charge+=1
				else:
					total_charge+= 0
			charge_score=(total_charge/len(residueList))
			#print(resnList, charge_score,total_charge)
			#weight+= charge_score
			weight+=total_charge
			weight+=periodicity_polarity(residueList)
			if weight >= 1.0:
				num_edge_strands+=1
			weight_dict[key] = weight
		#if num_edge_strands<2:
		#	print ("choosing another beta-strand as edge with larger weights")
		#sorted_weights = sorted(list(weight_dict.values()), reverse= True)
		sorted_weights = sorted(weight_dict.items(), key=operator.itemgetter(1))
		#print(sorted_weights) # sorted_weights is a tuple now
		for (key, value) in sorted_weights:
			
			resnList = ssSegmentDict[key]
			#print(resnList, value)
			if value >= 2.65:
				for resn in resnList:	
					bstrandDict[resn] = "E"
			else:
				for resn in resnList:	
					bstrandDict[resn] = "I"
		
		
		
def calc_ext_int_bstrand():
	HAList = []
	for key in ssSegmentDict:
		if key.startswith("B"):
			resnList = ssSegmentDict[key]
			residueList = ssSegmentResidueDict[key]
			if key in ssSegmentHADict:
				HAList = ssSegmentHADict[key]
			
			for resn in resnList:
				if resn in asaDict:
					if asaDict[resn] >= 0.25:
						ss = "Be"
					else: ss = "Bi"
				else:
					ss="B"
				ssDict[resn] = ss
			num1 = 0
			num2 = 0
			for resn in resnList:
				if ssDict[resn] == "Be":
					num1+=1
				elif ssDict[resn] == "Bi":
					num2+=1	
			#print(resnList, len(resnList), num1, num2)
			# calculate the ratio and assign the beta-strand to the the majority class
			#if num1/len(resnList)*100 >= 60:
			ratio1 = num1/(num1+num2)*100
			ratio2 = num2/(num1+num2)*100
			if  ratio1 >= 60:
				#print("Assigning to Be.....")
				
				for resn in resnList:
					ssDict[resn] = "Be"
			elif ratio2 >= 60:
				#print("Assigning to Bi?????")
				ext_strand_flag = check_altering_hydrophilic_hydrophobic(residueList)
				if ext_strand_flag == 0:
					for resn in resnList:
						ssDict[resn] = "Bi"
				else:
					for resn in resnList:
						ssDict[resn] = "Be"
					
			elif ratio1 == 50 or ratio2 == 50:
				# check the alternating Halpha secondary chemical shift pattern or the altenating hydrophilic or hydrophobic residue pattern in surface strand with the exception of two hydrophobic or hydrophilic residues occurence
				
				
				ext_strand_flag = check_altering_hydrophilic_hydrophobic(residueList)
				
				
				
					
						
				#if len(HAList) == len(resnList):
					
					#print ("checking HA sec. shifts", HAList)
					#print(detect_periodicity_sec_cs(HAList))
				if ext_strand_flag == 1:
					
					for resn in resnList:
						ssDict[resn] = "Be"
				else:
					
					for resn in resnList:
						ssDict[resn] = "Bi"
				

	
	
def calc_hydrophobic_moment():
	pass
	
def calc_ext_int_helix():
	for key in ssSegmentDict:
		if key.startswith("H"):
			resnList = ssSegmentDict[key]
			
			for resn in resnList:
				if resn in asaDict:
					if asaDict[resn] >= 0.25:
						ss = "He"
					else: ss = "Hi"
				else:
					ss="H"
				ssDict[resn] = ss
			num1 = 0
			num2 = 0
			for resn in resnList:
				if ssDict[resn] == "He":
					num1+=1
				elif ssDict[resn] == "Hi":
					num2+=1	
			
			# calculate the ratio and assign the beta-strand to the the majority class
			
			ratio1 = num1/(num1+num2)*100
			ratio2 = num2/(num1+num2)*100
			if  ratio1 >= 50:
				
				for resn in resnList:
					ssDict[resn] = "He"
			elif ratio2 >= 50:
				
				for resn in resnList:
					ssDict[resn] = "Hi"
					
			elif ratio1 == 50 or ratio2 == 50:
				pass
				


def recalc_ss_segments():
	
	num = -1
	segmentStart = 0
	
	for i in range(len(resn_res_array)):
		
		 
		current = resn_res_array[i]
		if i+1 < len((resn_res_array)):
			next = resn_res_array[i+1]
		
		if current in ssDict and next not in ssDict:
			if segmentStart:
				ssSegmentResNum[num] += [current]
				segmentStart = 0
			elif not segmentStart:
				num+=1
				ssSegments.append(ssDict[current][0])
				ssSegmentResNum[num] = [current]
		if current in ssDict and next in ssDict:
			
			if not segmentStart:
				num += 1
				
				ssSegments.append(ssDict[current][0])
				
				ssSegmentResNum[num] = [current]
				segmentStart = 1
				if ssDict[current][0] != ssDict[next][0] and segmentStart:
				
					
					segmentStart = 0
				
			elif (ssDict[current][0] == ssDict[next][0] and segmentStart):
				ssSegmentResNum[num] += [current]
			elif (ssDict[current][0] != ssDict[next][0] and segmentStart):
				ssSegmentResNum[num] += [current]
				segmentStart = 0
	
	return num	

def calc_bhairpin(num_segments):
	
	i = 0

	while( i < len(ssSegments)):
		
		#print(ssSegments[i], "???")
		# this allows the length of coil segment be <= 3 between two beta-strands connected by a beta-turn  
		if i+4 <len(ssSegments) and ((ssSegments[i]== "B" and len(ssSegmentResNum[i]) >= 3)  and ( (ssSegments[i+1]== "T" and ssSegments[i+2] == "C"  and len(ssSegmentResNum[i+2]) <= 3 and ssSegments[i+3] == "B") or (ssSegments[i+1]== "C" and len(ssSegmentResNum[i+1])<= 3 and ssSegments[i+2]== "T" and (ssSegments[i+3]== "B" and len(ssSegmentResNum[i+3]) >= 3)))):
			
			#print("\nfound beta-hairpin:::::::::\n")
			
			resnList0 = ssSegmentResNum[i]
			resnList0 += ssSegmentResNum[i+1]
			resnList0 += ssSegmentResNum[i+2]
			resnList0 += ssSegmentResNum[i+3]
			
			
		
			
			for resn in resnList0:
				bhpinDict[resn] = "P"
				#ssDict[resn] = ssDict[resn]+" BH"
				
		
			i = i+4
		elif  i+3 < len(ssSegments) and ((ssSegments[i]== "B" and len(ssSegmentResNum[i]) >= 3) and ssSegments[i+1]== "T" and (ssSegments[i+2] == "B"and len(ssSegmentResNum[i+2])>=3)):
			
			#print("\nfound beta-hairpin.........\n")
			resnList0 = ssSegmentResNum[i]
			resnList0 += ssSegmentResNum[i+1]
			resnList0 += ssSegmentResNum[i+2]
			
			for resn in resnList0:
				bhpinDict[resn] = "P"
				#ssDict[resn] = ssDict[resn]+" BH"
			
			i = i+3	
				
			
		else:
			resnList = ssSegmentResNum[i]
			for resn in resnList:
				bhpinDict[resn] = "NP" 
			i = i+1

def main():
	read_file()
	num_segments=calc_ss_segments()
	#check_extend_shrink_ss_boundaries()
	#num_segments=calc_ss_segments()
	#print(num_segments, "before...........")
	# copy down the ssDict before turn assignment
	ssDictOrig = ssDict.copy()
	calc_bturn()
	num_segments= recalc_ss_segments()
	#print(num_segments, "after...........")
	calc_bhairpin(num_segments)
	calc_edge_internal_strand()
	
	
	
	
	fout = open(outFile, "w")
	fout.write("#Resn Residue SS Edge/Interior Turn Bhpin\n")
	for i in range(len(resn_res_array)):
		key = resn_res_array[i]
		if key in ssDict:
                        #print(key, ssDict[key])
			(resn, residue)= key.split("_")[0], key.split("_")[1]
			
			
			fout.write ("%s, %s, %s" %(resn, residue, ssDictOrig[key]))
			
			if key in bstrandDict:
				fout.write(", %s"%bstrandDict[key])
			else:	
				fout.write(", NB")	
			if key in bturnDict:
				fout.write(", %s"%bturnDict[key])
			else:
				fout.write(", NT")	
			if key in bhpinDict:
				fout.write(", %s"%bhpinDict[key])
			else:
				fout.write(", ")
			fout.write("\n")	
	

# main function call here
main()	

