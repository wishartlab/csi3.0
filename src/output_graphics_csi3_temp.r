#!/usr/local/bin/Rscript

 Args <- commandArgs(TRUE)
 
 if (length(Args) < 1) {
   cat("\nError: No input file supplied.")
   cat("\n\t Syntax: output_graphics_latest.r <input-bmrb> \n\n")
   quit()
 }

infile = paste(Args[1], "csi3", sep=".")
#outfile = paste(Args[1], "png", sep=".")
outfile = paste(Args[1], "png", sep=".")

#Args[1] = "bmr16117_2KDM_GB95"
#filePrefix = "/Users/Noor/Documents/Research/Standalone_CSI3.0/A002_bmr6338"#A005_bmr6709, A010_bmr6198, A007_bmr6457, A019_bmr15249
#filePrefix = "/Users/Noor/Documents/Research/Standalone_CSI3.0/A010_bmr6198"
#infile = paste(filePrefix, "csi3", sep=".")
#outfile = paste(filePrefix, "png", sep=".")


data = read.table(file=infile,sep=' ', header=FALSE, blank.lines.skip=TRUE, comment.char="#")
resn = data[,1]
residue = data[,2]
predSS = data[,3]
predSupSS = data[,4]
tagSS = NULL
for (i in 1:length(predSS)){
  if (predSS[i] == "Be" ){
    tagSS[i] = -0.5
  }else if( predSS[i]=="Bi"){
    tagSS[i] = -0.75
  }else if (predSS[i] == 'H'){
    tagSS[i] = 0.5
  }else if (predSS[i] == 'C'){
    tagSS[i] = 0
  }else if (predSS[i] == "T1" ){
    tagSS[i] = 0.75
  }else if (predSS[i] == "T2"){ # T1p, T2p, T8 needs to be tagged
    tagSS[i] = 0.74
  }else if (predSS[i] == "T8" ){
    tagSS[i] = 0.73
  }else if (predSS[i] == "T1p" ){
    tagSS[i] = 0.72
  }else if (predSS[i] == "T2p" ){
    tagSS[i] = 0.71
  }
}

# n determines the splitting of the graph at every 50 residues
tagSSLen = length(tagSS)
tagSS[tagSSLen+1]=1 #inserting a trailing character to facilitate displaying the last secondary structure element
n = ceiling(tagSSLen/ 50)


if (tagSSLen < 100) {
  #png(filename=outfile, width=800, height=900)
  png(filename=outfile, width=850, height=700)
}else if (tagSSLen > 100 & tagSSLen <= 150) {
  png(filename=outfile, width=850, height=1000)
}else if (tagSSLen > 150 && tagSSLen < 200){
  png(filename=outfile, width=850, height=1200)
}else{
  png(filename=outfile, width=850, height=1700)
}
# number of columns in the plot
par ( mfcol=c(n+(n+1), 1), xpd=TRUE)

# bottom, left, top and right margins
par(mar=c(0.7,3,1.2,3))
#par(mar=c(1,1,1,1))
plot(c(1,50),c(1,5), type = "n", axes=FALSE, xlab="", ylab="")
legend("center", inset = 0, c("Edge Beta", "Interior Beta", "Helix", "Coil", "turn-I", "turn-II", "turn-I'", "turn-II'", "turn-VIII"), col=c("blue", "cyan","red", "black", "magenta", "green", "brown", "purple", "yellow"),lwd =5, cex =1.8, horiz=FALSE )


start = 1
res_end = start+50-1
xaxis_start = 0
xaxis_end = 0
bstrand_begin = 0
bturn_begin = 0
coil_begin = 0
helix_begin = 0
singleHelix = 0
singleCoil = 0
singleBeta = 0 
last_single_res_ss = 0
#--------------------------------------------------------
# Drawing the graphics and barplots for secondary structures
# alpha-helix = sine curve with xaxis*2.8 period
# exterior/interior beta-strand = arrow
# random -coil = straight line
# beta-turns = square waves made of 5 segments
#--------------------------------------------------------

bhpinstart = 0

for (i in 1:n){
  
  ##################################################################
  par(mar=c(3,3,4,3))
  #par(mar=c(0.5,0.5,0.2,0.2))
  p= plot(c(0.7,50 ), c(1, 10), type="n", axes=FALSE, xlab="", ylab="")
  #axis(1, at=p, tick=FALSE)
  x_axis = 0
  y_axis = 0
  num_bturn_res = 0
  for (j in start:res_end){
    current = j
    nextres = j+1
    x_axis = x_axis +1
    
    if (nextres <= res_end){
      #cat(resn[i], tagSS[j], tagSS[j+1], '........\n')
      
      # when a ss segment ends and next segment starts, then start drawing the previous one
      if (tagSS[j] != tagSS[j+1]){
        xaxis_end = x_axis
        
        if ( j+1 == res_end){
          
          
          
          #print(".................")
          #print(xaxis_start)
          #print(xaxis_end)
          if (tagSS [j+1] == 0.0){
            # coil
            segments(x_axis, 0,x_axis+1, 0, lwd=4)
            
          }else if (tagSS [j+1] == -0.5){
            # exterior beta-strand
            arrows(x_axis, 0, x_axis+1, 0, length=0.25, angle=30, lwd=18, col="blue")
          }else if (tagSS [j+1] == -0.75){
            # interior beta-strand
            arrows(x_axis, 0, x_axis+1, 0, length=0.25, angle=30, lwd=18, col="cyan")
          }else if (tagSS [j+1] == 0.5){
            # helix
            f = function(x) sin(x*2.8)*4 + 1                        
            curve(f, xlim=c(x_axis-0.8, x_axis+1+0.8), lwd=8, col="red", add=TRUE)
            
          }
        }
        
        # drawing the last residue secondary structure
        if (tagSS[j+1] == 1 && (tagSS[j-1] != tagSS[j])){
          
          last_single_res_ss = 1
          xaxis_start = xaxis_end
          xaxis_end = xaxis_start + 1 	
        }
        
        
        
        
        if ( j+1 != res_end && helix_begin == 0 && coil_begin == 0 && bstrand_begin == 0 && bturn_begin == 0){
          
          if (tagSS[j+1] == 0.0) {singleCoil = 1}
          else if (tagSS[j+1] == 0.5) {singleHelix = 1}
          else if (tagSS[j+1] == -0.5) {singleBeta = 1}
          xaxis_start = x_axis
          xaxis_end = xaxis_start + 1
        }	
        if (tagSS[j] == 0.5 ){
          
          # determining offset for drawing rectangular box or arrow when
          # there are some preceding random coils 
          
          if (singleHelix== 1){
            #rect(xaxis_start, -4, xaxis_end, 4, col = "red" )  
            f = function(x) sin(x*2.8)*4 + 1
            curve(f, xlim=c(xaxis_start-0.8, xaxis_end+0.8), lwd=8, col="red", add=TRUE ) 	
            singleHelix = 0   		
          }else if (last_single_res_ss == 1){
            
            f = function(x) sin(x*2.8)*4 + 1
            curve(f, xlim=c(xaxis_start-0.8, xaxis_end+0.8), lwd=8, col="red", add=TRUE )
            last_single_res_ss = 0
            
            
          }else{
            f = function(x) sin(x*2.8)*4+1	
            curve(f, xlim=c(xaxis_start-0.8, xaxis_end+0.8), lwd=8, col="red", add=TRUE )
          } 
          helix_begin = 0
        }else if(tagSS[j] == -0.5 ){
          if (last_single_res_ss == 1){
            arrows(xaxis_start, 0, xaxis_end, 0, length=0.25, angle=30, lwd=18, col="blue")
            last_single_res_ss = 0
          }else  if (singleBeta == 1){
            arrows(xaxis_start, 0, xaxis_end, 0, length=0.25, angle=30, lwd=18, col="blue")
            singleBeta = 0
          }else{
            arrows(xaxis_start, 0, xaxis_end, 0, length=0.25, angle=30, lwd=18, col="blue")
          }
          
          bstrand_begin = 0
        }else if(tagSS[j] == -0.75 ){
          if (last_single_res_ss == 1){
            arrows(xaxis_start, 0, xaxis_end, 0, length=0.25, angle=30, lwd=18, col="cyan")
            last_single_res_ss = 0
          }else if (singleBeta == 1){
            arrows(xaxis_start, 0, xaxis_end, 0, length=0.25, angle=30, lwd=18, col="cyan")
            singleBeta = 0
          }else{ # draw multiple residue beta-strands
            arrows(xaxis_start, 0, xaxis_end, 0, length=0.25, angle=30, lwd=18, col="cyan")
          }
          bstrand_begin = 0
          
        }else if( (tagSS[j] <= 0.75 && tagSS[j] >= 0.71) && bturn_begin == 1 ){ # drawing beta-turns
          
          if (tagSS[j] == 0.75){#bturn type I
            color = "magenta"
          }else if(tagSS[j] == 0.74){#bturn type II
            color = "green"
          }else if(tagSS[j] == 0.73){#bturn type VIII
            color = "brown"
          }else if(tagSS[j] == 0.72){#bturn type I'
            color = "purple"
          }else if(tagSS[j] == 0.71){#bturn type II'
            color = "yellow"
          }
          
          # drawing contiguous multiple beta-turns if any, otherwise draw a single one 
          num_bturns = (num_bturn_res+1) / 4 # +1 to count the last residue
          cat(num_bturn_res, num_bturns, "..........\n")
          x_start = xaxis_start 
          for (k in 1:num_bturns){
            # drawing beta-turns with four straight line segments
            segments(x_start, 0, x_start+1,0, lwd =4, col = color)
            segments(x_start+1,0, x_start+1,3, lwd =4, col = color  )
            segments(x_start+1,3, x_start+2,3, lwd =4, col = color  )
            segments(x_start+2,3, x_start+2,0, lwd =4, col = color )
            segments(x_start+2,0, x_start+3,0, lwd =4, col = color )
            x_start = xaxis_start + 4 
          }
          
          num_bturn_res = 0
          bturn_begin = 0
          
          
        }else if(tagSS[j] == 0.0 ){
          if (singleCoil== 1){
            
            #print(xaxis_start) 
            segments(xaxis_start, 0,xaxis_end, 0, lwd=4)
            singleCoil = 0
          }else if (last_single_res_ss == 1){
            #print(xaxis_start) 
            segments(xaxis_start, 0,xaxis_end, 0, lwd=4)
            last_single_res_ss  = 0
          }else{
            #print(xaxis_start)
            #print(xaxis_end)
            segments(xaxis_start, 0,xaxis_end, 0, lwd=4)
          }
          coil_begin = 0
        }
        
        
        
      }else if(tagSS[j] == tagSS[j+1] ){
        
        if (j+1 == res_end){
          xaxis_end = x_axis
          if (tagSS[j] == 0.5){
            f = function(x) sin(x*2.8)*4 + 1
            curve(f, xlim=c(xaxis_start-0.5, xaxis_end+0.5), lwd=8, col="red", add=TRUE )
            helix_begin = 0
          }else if (tagSS[j] == -0.5){
            arrows(xaxis_start, 0, xaxis_end, 0, length = 0.25, angle = 30, lwd=18, col="blue")
            bstrand_begin = 0
          }else if (tagSS[j] == -0.75){
            arrows(xaxis_start, 0, xaxis_end, 0, length = 0.25, angle = 30, lwd=18, col="cyan")
            bstrand_begin = 0
          }else if (tagSS[j] == 0){
            #print(xaxis_start)
            #print(xaxis_end)
            segments(xaxis_start, 0,xaxis_end, 0, lwd=4)
            coil_begin = 0
          }
          
        }else{	
          if(tagSS[j] == 0.5 && helix_begin != 1){
            xaxis_start = x_axis
            #print(xaxis_start)	
            
            helix_begin = 1
            
          }else if ((tagSS[j] == -0.5 || tagSS[j] == -0.75) && bstrand_begin != 1){
            xaxis_start =  x_axis
            bstrand_begin = 1
            
          }else if (tagSS[j] == 0.0 && coil_begin != 1){
            xaxis_start =  x_axis
            coil_begin = 1
          }else if (tagSS[j] <= 0.75 && tagSS[j] >= 0.71){
            num_bturn_res = num_bturn_res + 1
            # recording the the starting xaxis of a beta-turn region
            if (bturn_begin == 0){
              xaxis_start =  x_axis
            }  
            bturn_begin = 1
          }
        }
      }  
      
    }
  }
  
  
  
  #---------------------------------------------
  # drawing the barplot and residue numbering 
  #---------------------------------------------
  par(mar=c(3,3,3,3))
  #par(mar=c(0.5,0.5,0.2,0.2))
  #currentTagSS = tagSS[start:res_end]
  
  colors = vector()
  currentTagSS = vector()
  index = 1
  for (l in start:res_end){
    
    if (tagSS[l] == 0.5  ){ # helix
      currentTagSS[index] = 0.75
    }else if( tagSS[l] == -0.5 | tagSS[l] == -0.75){ # exterior/ interior strand
      currentTagSS[index] = -0.75
    }else{ currentTagSS[index] = 0.0} # coil and different turns
    
    if (tagSS[l] == 0.5){
      colors[index] = 'red'
    }else if (tagSS[l] == -0.5){
      colors[index] = 'blue'
    }else if (tagSS[l] == -0.75){
      colors[index] = 'cyan'
    }else if (tagSS[l] == 0.0 | (tagSS[l] >= 0.71 && tagSS[l] <= 0.75)){
      colors[index] = 'black'
    } 
    index = index+1
  }
  
  currentTagSSLen=length(currentTagSS)
  #cat(currentTagSSLen)
  
  if (currentTagSSLen < 50){
    # if the last vector of residues has less than 50 entries then
    # fill up the rest of the positions with white bars without border
    interval = 1/(50 -currentTagSSLen)
    newItems = seq(-0.6, 0.4, by=interval)
    currentTagSS=c(currentTagSS,newItems)# pasting the rest items to currentTagSS to make it a length of 50
    cat(length(colors), length(currentTagSS), "\n")
    for (k in length(colors): length(currentTagSS)){
      colors [k] = 'white'
    } 
    
    #col= rgb(1, 0, 0,0.5) --> the 4th argument is for transparency
    #d= barplot(currentTagSS, col=ifelse(currentTagSS==0.5 ,"red", ifelse(currentTagSS==-0.5,"blue",ifelse(currentTagSS==-0.75, "cyan", 
    #ifelse((currentTagSS==0.0),"black", "white")))),axes=FALSE, xaxt='n', 
	#border=ifelse((currentTagSS==0.5|currentTagSS==-0.5 |currentTagSS==0.0 |currentTagSS==0.75 |currentTagSS==-0.75 ),TRUE,NA ))
   
    d = barplot(currentTagSS, col= colors, axes=FALSE, xaxt='n', border=ifelse(colors=='white',NA, TRUE))
    text(-2,0, labels=c(resn[start]), cex=1.8)
    text(currentTagSSLen+12,0, labels=c(resn[res_end]), cex =1.8)
    #mtext (start, side=2)
    #mtext (res_end, side=4)
  }
  
  else{
	  #d= barplot(currentTagSS, col=ifelse(currentTagSS==0.5 ,"red", ifelse(currentTagSS==-0.5,"blue",ifelse(currentTagSS==-0.75, "cyan", 
	 #ifelse((currentTagSS==0.0),"black", "white")))),axes=FALSE, xaxt='n', border=TRUE )
    d = barplot(currentTagSS, col=colors,axes=FALSE, xaxt='n', border=TRUE )
    #mtext (start, side=2, padj = 0)
    #mtext (res_end, side=4, padj = 0)
    text(-2,0, labels=c(resn[start]), cex=1.8)  # printing the start residue label
    text(50+12,0, labels=c(resn[res_end]), cex =1.8) # printing the end resiue label
  }
  
  
  # re-setting the start and last residue number
  start = res_end+1
  if (start+50-1 > tagSSLen){
    res_end = tagSSLen
    #print(start)
    #print(res_end)
  }else{
    res_end = start+50-1
  }
  
  
}

dev.off()

