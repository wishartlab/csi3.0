#!/usr/bin/python
import sys

three2one ={'ALA' : 'A','ARG' : 'R','ASN' : 'N','ASP' : 'D','ASX' : 'B','CYS' : 'C','GLN' : 'Q',
	     'GLU' : 'E','GLX' : 'Z','GLY' : 'G','HIS' : 'H','ILE' : 'I','LEU' : 'L','LYS' : 'K',
	     'MET' : 'M','PHE' : 'F','PRO' : 'P','SER' : 'S','THR' : 'T','TRP' : 'W','TYR' : 'Y',
	     'VAL' : 'V','PCA' : 'X'}


inFile = sys.argv[1]


def createSSPredHTMLText():
	CSI_pred="%s.finalpred" % (inFile)
	file_open = open(CSI_pred, "r")
	pred_ss_line = file_open.readlines()
	
		
	pred_ss_line="<br>".join(pred_ss_line)
	file_open.close()
		
	with open(inFile+".sspred.html", "w") as fout:
		fout.write ("<link href='txtstyle.css' rel='stylesheet' type='text/css' /> %s" % pred_ss_line )
	with open("txtstyle.css", "w") as f:
		f.write ("html, body {font-family:Courier}")

def main():
	
	fin = open(inFile+".csi3", "r")
	flines = fin.readlines()
	proteinLen = len(flines)
	SupSSstring = ' '* proteinLen
	SupSSstring = list(SupSSstring)
	resnList = []
	AAstring = []
	SSstring = []
	SShtmlString = []

	bhpinstart = 0
	bturnstart = 0
	bstrandstart = 0
	helixstart = 0
	
	tagged = 0
	index = 0
	for i in range(0, len(flines)-1):
		currentLine = flines[i].strip()
		nextLine = flines[i+1].strip()
		#print(currentLine)
		if currentLine.startswith("#"): continue
		
		(resn, aa, ss, edge, turn, bhpin) = currentLine.split(", ")
		(nextresn, nextaa, nextSS, nextEdge, nextTurn, nextBhpin) = nextLine.split(", ")
		
		#print(ss, edge, turn, bhpin)
			
		currentSS = ss
		currentEdge = edge
		currentTurn = turn
		currentBhpin = bhpin 
		currentResn = int(resn)
		resnList.append(int(resn))
		AAstring.append(three2one[aa])
		
		if ss == "H" and helixstart == 0 : 
			helixstart = 1
			
			if tagged == 0:
				SShtmlString += ["<font color=\"red\">"]
				tagged = 1
			SShtmlString+=["H"]	
		elif helixstart == 1 and currentSS == nextSS:		
			SShtmlString+=["H"]	
		elif helixstart == 1 and currentSS != nextSS:
			SShtmlString+=["H"]
			helixstart = 0
			tagged = 0
			SShtmlString+=["</font>"]
		if ss == "C" and currentTurn == "NT" and tagged == 0:
			#print("C??????????")
			SShtmlString+=["C"]
		if ss == "B" and bturnstart == 0 and bstrandstart == 0 : 
			bstrandstart = 1
			if edge == "E" and tagged == 0:
				SShtmlString += ["<font color=\"blue\">"]
				tagged = 1
			elif edge == "I" and tagged == 0:
				SShtmlString += ["<font color=\"cyan\">"]
				tagged = 1
			#print("Entering bstrand...........")	
			SShtmlString+=["B"]	
		elif bstrandstart == 1 and currentSS == nextSS and currentTurn == "NT" :		
			SShtmlString+=["B"]
		elif bstrandstart == 1 and (nextTurn.startswith("T") or currentSS!= nextSS):
			SShtmlString+=["B"]
			#print(resn, ".......................")
			bstrandstart = 0
			tagged = 0
			SShtmlString+=["</font>"]
		if currentTurn.startswith("T") and currentSS != "B" and bturnstart == 0 : 
			bturnstart = 1
			if turn[-1] == "1" and tagged == 0:
				SShtmlString += ["<font color=\"magenta\">"]
				tagged = 1
			elif turn[-1] == "2" and tagged == 0:
				SShtmlString += ["<font color=\"green\">"]
				tagged = 1	
			elif turn[-2:] == "1p" and tagged == 0:
				SShtmlString += ["<font color=\"purple\">"]
				tagged = 1	
			elif turn[-2:] == "2p" and tagged == 0:
				SShtmlString += ["<font color=\"yellow\">"]
				tagged = 1	
			elif turn[-1] == "8" and tagged == 0:
				SShtmlString += ["<font color=\"brown\">"]
				tagged = 1
			SShtmlString+=["T"]	
		elif bturnstart == 1 and (currentTurn == nextTurn) and (currentSS != "B"):
			#print(resn, "::::::::")
			SShtmlString+=["T"]
		elif bturnstart == 1 and nextTurn == "NT" and currentSS != "B" :
			#print(resn, ">>>>>>>>>>>>>>>>")
			SShtmlString+=["T"]
			bturnstart = 0
			SShtmlString+=["</font>"]
			tagged = 0
		elif bturnstart == 1 and currentSS == "B" :
			#print(resn, ",,,,,,,,,,,,,,,")
			SShtmlString+=[""]
			bturnstart = 0
			SShtmlString+=["</font>"]
			tagged = 0
			
		# beta-hairpin tagging	
		if bhpinstart == 0 and currentBhpin == "P" :
			bhpinstart = 1
			start = index
		if bhpinstart == 1 and currentBhpin != nextBhpin:
			bhpinstart = 0
			end = index+1
			bhpinlen = end - start + 1
			if bhpinlen >= 10 and bhpinlen <= 15:
				SupSSstring [start:start+3] = "|--"
				SupSSstring [start+4:start+13] = "B-hpin"
				SupSSstring [end-3: end+1] = "--|"
			if bhpinlen > 15:
				SupSSstring [start:start+5] = "|--"
				SupSSstring [start+6:start+14] = "B-hpin"
				SupSSstring [end-3: end+1] = "--|"	
			#supss = "beta-hairpin"
		
		#prevSS = currentSS
		#prevSupSS = currentSupSS
		index += 1
		
		# appending the last residue information
		if i+1 == len(flines)-1:
			SShtmlString+=[nextSS]
			SSstring+=[nextSS]
			resnList+=[int(nextresn)]
			AAstring+=[three2one[nextaa]]
			SupSSstring +=" "
			
	if tagged == 1:
			SShtmlString+=["</font>"]		
	#print("".join(SShtmlString))
	
	
	output = ''
        font_color = ""
	proteinLen = len(resnList)
	j = 0
	k = 0
	for i in range(0,len(resnList), 50):
		SShtml = ""	
		SupSShtml = ""
		
                if i >= 50: SShtml+=font_color# to start the font tag at the beginning of each 50 residue line
		start = i        
		end = start + 50-1
		if end > proteinLen:            
			end = proteinLen-1
		#print(end)	

		
		count = 0
		font_tag_end = 0
		while (count <= 49 and j < len(SShtmlString)):
			
			if SShtmlString[j].startswith("<"):
				if SShtmlString[j].startswith("<font color"):
					font_color= SShtmlString[j]
					font_tag_end = 0
				elif SShtmlString[j].startswith("</font"):
					font_color= ''
					font_tag_end = 1
				SShtml+= SShtmlString[j]
				
			else:	
				SShtml+= SShtmlString[j]
				count+=1
		
			j+=1
		
		if font_tag_end == 0:	
			SShtml+="</font>" # to end the font tag in html after each 50 residue 	
		
			
		count = 0
		while (count <= 49 and k < len(SupSSstring)):
			
			if SupSSstring[k]==" ":
				SupSShtml+= "&nbsp;"
				
			else:	
				SupSShtml+= SupSSstring[k]
			count+=1
			k+=1	
		#print(count)	
		output+="%d\t%s\t%d\n" %(resnList[start], "".join(AAstring[start:end+1]), resnList[end])
		#output+="%d\t%s\t%d\n" %(resnList[start], "".join(SSstring[start:end+1]), resnList[end])
		output+="%d\t%s\t%d\n" %(resnList[start], SShtml, resnList[end])
		output+="%d\t%s\t%d\n" %(resnList[start], SupSShtml, resnList[end])
		output+='\n'
	#print(output)	
	finalpredfile = inFile+'.finalpred'
	with open(finalpredfile, "w") as fout:
		fout.write(output)
	createSSPredHTMLText()	
	
main()	
