#!/usr/local/bin/python3
#********************************************
# Secondary Structure prediction main program                             
# - accepts commanl-line options
# Available options:
# a. use Re-referencing
# b. psipred flag
# c. Hidden Marcov model or HMM filtering in 
#    the final prediction
#  
#- output:
#  a. text file with predicted Secondary 
#  structure
#  b. graphical image  
#*********************************************
import os, sys, getopt, re, fnmatch
#from optparse import OptionParser
#from argparse import ArgumentParser
import getopt
import os.path
import subprocess



# global variables
HOME = "/apps/csi3/project/project"
CSI3_DIR = HOME+"/Standalone_CSI3.0"
SOFTWARES = HOME+"/softwares"
PANAV = "/apps/csi/project/Standalone_CSI2.0/PANAV-v2b.jar"
BLAST =  SOFTWARES+"/blast-2.2.26"
PSIPRED3 = SOFTWARES+"/psipred3.5"
RCI = CSI3_DIR+"/Standalone_CSI2.0/rci-latest"
SC_RCI = CSI3_DIR+"/Standalone_ShiftASA/Side_chain_RCI"
SABLE = CSI3_DIR+"/Standalone_ShiftASA/sable_distr"
RSCRIPT_EXEC = "/usr/bin/Rscript"
SCON_SRC = CSI3_DIR+"/Standalone_CSI2.0/conservation_code"
CSI2_SRC = CSI3_DIR+"/Standalone_CSI2.0/src"
CSI3_SRC = CSI3_DIR+"/src"
SHIFTASA_SRC = CSI3_DIR+"/Standalone_ShiftASA/src"
TALOSP_DIR = SOFTWARES+"/nmr/talosplus/bin"
TALOSN_DIR = SOFTWARES+"/nmr/talosn/bin"

def run_csi3 (options):

    
    

    print("# CSI3.0 Secondary Structure Prediction")
    usage = "usage %prog -i input_bmrb [options]"
    
    
    for option, value in options:
        if option == "-i":
            infile = value
            os.system ("echo %s >> csi3_log" %infile)
        elif option == "-n":
            NRC_FLAG = value 
            os.system ("echo %s >> csi3_log" %NRC_FLAG)
        elif option == "-m":
            FILE_FORMAT = value
        elif option == "-p":
            USE_PSIPRED = value
        elif option == "-f":
            FILTERING = value
        elif option == "-g":
            GRAPHICS = value
        elif option == "-x":
            RCI_FLAG = value
        elif option == "-r":
            REREF = value    
    
    WORKDIR = os.getcwd()
    
    
    if USE_PSIPRED == "y" or USE_PSIPRED == "yes":
        PSIPRED_FLAG = "psipredyes"
    else:
        PSIPRED_FLAG = "psipredno" 

    window_size = ''
    
    if FILTERING == '5res' or FILTERING == '7res':
        FILTER_FLAG = "yes"
        window_size = int(FILTERING[0])
    else:
        FILTER_FLAG = "no"
    
    
    #REREF = "no"    
    input_bmrb_id = infile.split(".")[0]
    
    #======================================================================
    # pre-processing the data
    #======================================================================
    
    if REREF == "yes" or REREF == "y" :
        #call PANAV-v2b for chemical-shift rereferencing in batch mode
        
        if FILE_FORMAT == "bmrb3" or FILE_FORMAT == "shifty" :
            REREF = "no"
        elif FILE_FORMAT == "bmrb2" :
            os.system("%s/run_PANAV.sh" %CSI2_SRC)    
            os.system("echo 'running panav2talos...' >> csi3_log")
            if os.path.exists(input_bmrb_id+".out_calibrated"):
                os.system("%s/bmrb2talosV2.0.com %s > %s.tab" %(CSI2_SRC, infile,input_bmrb_id))    
                os.system("python3 %s/panav2talos.py %s >> csi3_log" %(CSI2_SRC, input_bmrb_id))
            else:
                os.system("echo 'failed to run panav...running bmrbtotalos converter'>> csi3_log")
                os.system("%s/bmrb2talosV2.0.com %s > %s.tab" %(CSI2_SRC, infile,input_bmrb_id))                
    else:
        if FILE_FORMAT == "bmrb2" :#bmrb 2.1 format
            os.system("echo 'running bmrb2talos...' >> csi3_log")   
            os.system("%s/bmrb2talosV2.0.com %s > %s.tab" %(CSI2_SRC, infile,input_bmrb_id))
        elif FILE_FORMAT == "bmrb3" :#bmrb 3.1 format
            os.system("echo 'running bmrb3toTalos...' >> csi3_log")   
            os.system("python3 %s/bmrb3toTalos.py %s > %s.tab" %(CSI2_SRC, infile,input_bmrb_id))    
        elif FILE_FORMAT == "shifty": #shifty format
            # shifty -to- talos conversion and sequence extraction in fasta format
            os.system("echo 'running shifty2talos...' >> csi3_log")
            os.system("python %s/shifty2talos.py %s" %(CSI2_SRC, infile))
            
    #to indicate the talos format conversion is done
    os.system("touch donetalos")
    
                    
    # secondary chemical shift calculation with neighbor residue correction
    if NRC_FLAG == "yes" or NRC_FLAG == "no":
        os.system("echo 'running neighbor residue correction...' >> csi3_log")
        os.system("perl %s/seccs_calculation_with_nrc.pl %s.tab %s >> csi3_log" %(CSI2_SRC, input_bmrb_id, NRC_FLAG))
        os.system("echo 'mv *.nrc *.tab...' >> csi3_log")
        os.system("mv %s.nrc %s.tab >> csi3_log" %(input_bmrb_id, input_bmrb_id))
    
    #sequence extraction in fasta format
    os.system("echo 'running parse_bmrb_seq ...\n' >> csi3_log")
    os.system("python3 %s/parse_bmrb_seq.py %s.tab >> csi3_log "%(CSI2_SRC, input_bmrb_id)) 
   
    # filling up the missing chemical shift entries
    os.system("echo 'running matching triplet and filling the gap in assignment...' >> csi3_log")
    os.system("python3 %s/match_triplet_V2.py %s.tab >> csi3_log " %(CSI2_SRC, input_bmrb_id))
    
    #======================================================================
    # feature calculation
    #======================================================================

    # ss_probability
    os.system("echo 'running secondary structure probability...' >> csi3_log")
    os.system("perl %s/ssp_calculation.pl %s.tab >> csi3_log" %(CSI2_SRC,input_bmrb_id))

    # Random Coil Index value
    os.chdir(RCI)
    os.system("echo 'in the rci folder...%s' >> %s/csi3_log" %(os.getcwd(),WORKDIR))
    os.system("echo 'in the rci folder...%s' >> csi3_log_david_test" %(os.getcwd()))
    if FILE_FORMAT == "bmrb2": 
        command ="python %s/rci-prog-updated.py -b2 %s/%s" %(os.getcwd(), WORKDIR, infile)
    if FILE_FORMAT == "bmrb3": 
        command ="python %s/rci-prog-updated.py -b3 %s/%s" %(os.getcwd(), WORKDIR, infile)    
    elif FILE_FORMAT == "shifty" :#flag = -sh
        command ="python %s/rci-prog-updated.py -sh %s/%s" %(os.getcwd(), WORKDIR, infile)   
    os.system(command)
    os.chdir(WORKDIR)
    
    #Side-chain RCI value
    os.chdir(SC_RCI)
    os.system("echo 'in the side-chain rci folder...%s' >> %s/csi3_log" %(os.getcwd(),WORKDIR))
    if FILE_FORMAT == "bmrb2":
        command ="python side_chain_rci_updated.py -b2 %s/%s" %(WORKDIR, infile)
    if FILE_FORMAT == "bmrb3":
        command ="python  side_chain_rci_updated.py -b3 %s/%s" %(WORKDIR, infile)
    #elif FILE_FORMAT == "shifty" :#flag = -sh
        #command ="python  side_chain_rci_updated.py -sh %s/%s" %(WORKDIR, infile)
    os.system(command)
    if FILE_FORMAT == "bmrb2" or FILE_FORMAT=="bmrb3":
        os.system("mv %s.Side_chain_RCI.txt %s/" %(infile, WORKDIR))
    elif FILE_FORMAT == "shifty":
        os.system("cp %s/%s_BMRB.RCI.txt %s/%s_BMRB.Side_chain_RCI.txt" %(WORKDIR, infile, WORKDIR, infile))    
    os.system("rm %s*" %(input_bmrb_id))
    os.chdir(WORKDIR)

    #PSIPRED run 
 
    if PSIPRED_FLAG == "psipredyes":
        # psipred predicted ss
        os.system("echo 'running PSIPRED3...' >> csi3_log")
        if not os.path.exists("%s.ss"%input_bmrb_id):
            os.system("%s/runpsipred %s.fasta >> csi3_log" %(PSIPRED3, input_bmrb_id))
   
        os.system("python3 %s/mapping_psipred_resnumbers.py %s.tab >> csi3_log"%(CSI2_SRC, input_bmrb_id) )

    # conservation score

    os.chdir(SCON_SRC)
    os.system("echo 'running conservation score...' >> csi3_log")

    os.system("perl %s/run_conservation_score.pl %s %s %s >> csi3_log" %(CSI2_SRC, input_bmrb_id, BLAST, WORKDIR))    
    os.chdir(WORKDIR)

    if not os.path.exists("%s.scon"%input_bmrb_id):
        SCON_FLAG = "sconno"
    else:
        SCON_FLAG = "sconyes"
    # predicted RSA and buried-exposed state
    os.system("echo 'predicting ASA...' >> csi3_log")
    command = "%s/predictASAf.sh %s %s" %(CSI2_SRC, input_bmrb_id, SCON_FLAG)
    os.system("echo '" + command + "' >> csi3_log")
    os.system(command)

    #======================================================================
    # final SS prediction
    #======================================================================
        
    os.system("echo 'creating ss feature file and predicting ss...' >> csi3_log")
   
    command = "%s/predictSS.sh %s %s %s" %(CSI2_SRC, input_bmrb_id, PSIPRED_FLAG, SCON_FLAG)
    os.system("echo '" + command + "' >> csi3_log")
    os.system(command)
    

    #======================================================================
    # Markov model filtering
    #======================================================================	
    if FILTER_FLAG == "yes" and window_size:     
        #print(window_size)
        os.system("echo 'running %s window HMM filtering ...' >> csi3_log" % window_size)
        os.system("python3 %s/N-gram_markov_model_filtering.py %s %s > %s.sspred.filt" %(CSI2_SRC, input_bmrb_id, window_size, input_bmrb_id))

        os.system("mv %s.sspred.filt %s.sspred1" %(input_bmrb_id,input_bmrb_id))

    #calculate side-chain rci and sable feature here
    #copy *.aln and mat file from psipred folder to OUT_SABLE sub-dir in sable dir before calling sable, it will save one psiblast run time inside sable
    #======================================================================
    # SABLE prediction    
    #====================================================================== 
    if not os.path.exists("%s.sable" %input_bmrb_id):
        os.chdir(SABLE)
        os.system("echo 'in the SABLE folder...%s' >> %s/csi3_log" %(os.getcwd(),WORKDIR))
        if os.path.exists("OUT_SABLE_RES") or os.path.exists("OUT_SABLE_graph"):
            os.system("rm -r OUT_SABLE*")
        os.system("cp %s/%s.fasta data.seq" %(WORKDIR, input_bmrb_id))
        os.system("cp %s/%s.aln align.out" %(WORKDIR, input_bmrb_id))
        os.system("cp %s/mat mat" %(WORKDIR))
        command = "./run.sable"
        os.system(command)
        os.system("mv OUT_SABLE_RES %s/%s.sable" %(WORKDIR, input_bmrb_id))

        os.chdir(WORKDIR)    
    #======================================================================
    # final ASA prediction    
    #======================================================================
    prediction_model_to_choose_flag = 'sable_scrci'
    SCRCI = 'yes'
    os.system("echo 'predicting ASA...' >> csi3_log")
    command = "%s/predictASAf.sh %s %s %s" %(SHIFTASA_SRC, input_bmrb_id, prediction_model_to_choose_flag, SCRCI)
    os.system("echo '" + command + "' >> csi3_log")
    os.system(command)        
    
    #======================================================================
    # torsion angle prediction
    #======================================================================
    os.chdir(TALOSP_DIR)
    
    os.system("cp %s/%s %s" %(WORKDIR, infile, input_bmrb_id )) 
    os.system("./run_talosp.com %s %s" %( input_bmrb_id, FILE_FORMAT)) 
    os.system("mv pred.tab %s/%s.anglepred"%(WORKDIR, input_bmrb_id))
    os.system("rm %s*" %input_bmrb_id)
    os.chdir(WORKDIR)

    #======================================================================
    # Beta-trun identification
    #====================================================================== 
    os.system("python %s/calc_superSS.py %s"%(CSI3_SRC, input_bmrb_id))
    os.system("python %s/finalSSCalc.py %s"%(CSI3_SRC, input_bmrb_id)) 
    #======================================================================
    # Output Graphics
    #======================================================================    
    if GRAPHICS == "y" or GRAPHICS == "yes": 
        # output graphics
        os.system("echo 'producing graphics...' >> csi3_log")
        
        command = "%s/output_graphics_csi3.sh %s" %(CSI3_SRC, input_bmrb_id)
        os.system(command)
        
    final_prediction_file = input_bmrb_id+'.csi3'     
    if os.path.exists(final_prediction_file):    
        os.system (" echo 'CSI3.0 is successfully done! The predicted secondary structure is in %s' > %s.success" %(final_prediction_file,input_bmrb_id))
    
def main():
    
    
    try:
        #Example: python3 predictSS.py -i bmrb -r no -n yes -m bmrb -p yes -f no -g yes -x no
        opts, args = getopt.getopt(sys.argv[1].split(), 'i:r:n:m:p:f:g:x:')
        
        # parsing arguments for command-line execution
        #opts, args = getopt.getopt(sys.argv[1:], 'i:r:n:m:p:f:g:x:')
        #os.system ("echo %s >> csi3_log" %sys.argv[1:])
        
        
        
        
    except getopt.GetoptError as err:
        os.system("echo %s >> csi3_log" %str(err))
        os.system("echo %s >> csi3_log" %usage)
        sys.exit(2)
   
    run_csi3(opts)
 	 
    	
main()
